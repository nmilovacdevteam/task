<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'front_page' );

$fields->setLocation( 'post_type', '==', 'page' )
	->and( 'page_type', '==', 'front_page' );

try {
	$fields
		;
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/frontpage.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;

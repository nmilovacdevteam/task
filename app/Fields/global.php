<?php

namespace App;

use StoutLogic\AcfBuilder\FieldNameCollisionException;
use StoutLogic\AcfBuilder\FieldsBuilder;

$fields = new FieldsBuilder( 'global_options' );

$fields->setLocation( 'options_page', '==', 'acf-options-globalna-podesavanja' );

try {
	$fields
		->addTab( 'op&scaron;te', [ 'placement' => 'left' ] )
		->addImage( 'logo', [
			'label'         => __( 'Logo sajta', THEME_TEXT_DOMAIN ),
			'wrapper'       => [
				'width' => '30',
			],
			'return_format' => 'url'
		] )
		->addImage( 'logo_dark', [
			'label'         => __( 'Logo sajta - tamni', THEME_TEXT_DOMAIN ),
			'wrapper'       => [
				'width' => '30',
			],
			'return_format' => 'url'
		] )
		->addImage( 'placeholder', [
			'label'   => __( 'Zamenska slika', THEME_TEXT_DOMAIN ),
			'wrapper' => [
				'width' => '30',
			]
		] );
} catch ( FieldNameCollisionException $e ) {
	printf( __( 'Došlo je do greške u pokušaju da registrujemo ACF polja u fields/global.php: %s', THEME_TEXT_DOMAIN ), $e );
}

return $fields;

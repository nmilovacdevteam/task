<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Theme assets
 */
add_action( 'wp_enqueue_scripts', function () {
	wp_enqueue_script( 'main.js', asset_path( 'scripts/main.js' ), [ 'jquery' ], null, true );

	$css_file        = 'styles/';

	switch ( true ) {
		case is_front_page():
			$css_file .= 'front-page.css';
			break;
		default:
			$css_file .= 'main.css';
	}

	$main_css_path = asset_path( $css_file );
	echo "<link rel='preload' href='$main_css_path' as='style'><link rel='stylesheet' href='$main_css_path'>";

	/**
	 * Dequeue unnecessary styles and scripts
	 */
	wp_dequeue_style( 'wc-block-style' );
	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_script( 'comment-reply' );


}, 100 );

/**
 * Theme setup
 */
add_action( 'after_setup_theme', function () {
	/**
	 * Enter theme text domain
	 */
	define( 'THEME_TEXT_DOMAIN', 'librafire' );


	/**
	 * Custom shortcodes
	 */
	add_shortcode( 'youtube', 'youtube_embed' );


	/**
	 * Enable features from Soil when plugin is activated
	 * @link https://roots.io/plugins/soil/
	 */
	add_theme_support( 'soil', [
		'clean-up',
		'disable-asset-versioning',
		'disable-trackbacks',
		'js-to-footer',
		'nice-search',
	] );

	/**
	 * Enable plugins to manage the document title
	 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
	 */
	add_theme_support( 'title-tag' );

	/**
	 * Register navigation menus
	 * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
	 */
	register_nav_menus( [
		'primary_navigation' => __( 'Glavna navigacija', THEME_TEXT_DOMAIN )
	] );

	/**
	 * Enable post thumbnails
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * Enable HTML5 markup support
	 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
	 */
	add_theme_support( 'html5', [ 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ] );

	/**
	 * Enable selective refresh for widgets in customizer
	 * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
	 */
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Use main stylesheet for visual editor
	 * @see resources/assets/styles/layouts/_tinymce.scss
	 */
	//add_editor_style( asset_path( 'styles/main.css' ) );

	/**
	 * Defining image sizes globally
	 */
	add_image_size( 'product_shop', 600, 600, true );
}, 20 );

/*
 * Add custom code to head
 */
add_action( 'wp_head', function () {
	$site_url     = site_url();
	$favicon_path = asset_path( 'images/favicon/' );
	// Font preloading, preload only necessary fonts
	$fonts = [
		asset_path( 'fonts/Urbanist-Regular.woff2' ),
		asset_path( 'fonts/Urbanist-Bold.woff2' ),
	];
	foreach ( $fonts as $font ) {
		echo "<link rel='preload' href='$font' as='font' type='font/woff2' crossorigin>";
	}
	?>
    <meta http-equiv="x-dns-prefetch-control" content="on">
    <link rel='dns-prefetch' href='<?php echo $site_url; ?>'/>
    <link rel='dns-prefetch' href='//code.jquery.com' crossorigin="anonymous"/>
    <link rel='dns-prefetch' href='//s.w.org' crossorigin="anonymous"/>

    <link rel="shortcut icon" href="<?php echo $favicon_path; ?>/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $favicon_path; ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $favicon_path; ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $favicon_path; ?>/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $favicon_path; ?>/site.webmanifest">
    <link rel="mask-icon" href="<?php echo $favicon_path; ?>/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="theme-color" content="#ffffff">
	<?php
} );


/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action( 'the_post', function ( $post ) {
	try {
		sage( 'blade' )->share( 'post', $post );
	} catch ( \Exception $e ) {
		printf( __( 'Došlo je do greške u app/setup.php:223: %s', THEME_TEXT_DOMAIN ), $e );
	}
} );

/**
 * Setup Sage options
 */
add_action( 'after_setup_theme', function () {
	/**
	 * Add JsonManifest to Sage container
	 */
	try {
		sage()->singleton( 'sage.assets', function () {
			return new JsonManifest( config( 'assets.manifest' ), config( 'assets.uri' ) );
		} );
	} catch ( \Exception $e ) {
		printf( __( 'Došlo je do greške u app/setup.php:238: %s', THEME_TEXT_DOMAIN ), $e );
	}

	/**
	 * Add Blade to Sage container
	 */
	try {
		sage()->singleton( 'sage.blade', function ( Container $app ) {
			$cachePath = config( 'view.compiled' );
			if ( ! file_exists( $cachePath ) ) {
				wp_mkdir_p( $cachePath );
			}
			( new BladeProvider( $app ) )->register();

			return new Blade( $app['view'] );
		} );
	} catch ( \Exception $e ) {
		printf( __( 'Došlo je do greške u app/setup.php:248: %s', THEME_TEXT_DOMAIN ), $e );
	}

	/**
	 * Create @asset() Blade directive
	 */
	try {
		sage( 'blade' )->compiler()->directive( 'asset', function ( $asset ) {
			return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
		} );
	} catch ( \Exception $e ) {
		printf( __( 'Došlo je do greške u app/setup.php:264: %s', THEME_TEXT_DOMAIN ), $e );
	}
} );

/**
 * Initialize ACF Builder
 */
add_action( 'init', function () {
	// Remove unwanted image sizes
	remove_image_size( '1536x1536' );
	remove_image_size( '2048x2048' );

	if ( function_exists( 'acf_add_local_field_group' ) ) {
		try {
			collect( glob( config( 'theme.dir' ) . '/app/Fields/*.php' ) )->map( function ( $field ) {
				return require_once( $field );
			} )->map( function ( $field ) {
				if ( $field instanceof FieldsBuilder ) {
					acf_add_local_field_group( $field->build() );
				}
			} );
		} catch ( \Exception $e ) {
			printf( __( 'Došlo je do greške u inicijalizaciji ACF Builder-a (app/setup.php:274): %s', THEME_TEXT_DOMAIN ), $e );
		}
	}
} );

<?php

namespace App\Traits;


trait Image {
	/**
	 * This functions composes full image html, along with srcset and defferd srcset if needed. Function should be
	 * called in picture tag, because it also returns webp images if any.
	 * Returns HTML code {string} => <img src="" srcset="image_url 300w, image_url_2 450w"
	 * sizes="(min-width: 992px) 300px, (max-width: 991px) and (min-width: 768px) 450px" alt="image_alt or post_title">
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public static function getImage( array $args = [] ) {
		$added_image_sizes = false;
		$image_width       = null;
		$image_height      = null;
		$images            = [];
		$webp_html         = null;
		$image_id          = null;
		$defaults          = [
			'start_to_deffer' => 0,     // Number
			'current_item'    => 0,     // Number
			'picture_classes' => '',    // String with classes
			'image_classes'   => '',    // String with classes
			'post_id'         => null,  // 1
			'image_id'        => null,  // Image id
			'term_id'         => null,  // Term id for thumbnail
			'image_sizes'     => '',    // string 'image_size' if no multiple image sizes otherwise array if multiple sizes ['size_one', 'size_two']
			'media_queries'   => [],    // ['(min-width: 992px)' => '500px']
			'is_carousel'     => false, // Check for carousel for correct deffering of the images
			'placeholder'     => true   // If true, function will look for image placeholder from global ACF Field get_field('placeholder', 'option')
		];
		$args              = (object) wp_parse_args( $args, $defaults );

		// Check from witch object should we get image
		switch ( true ) {
			case ! is_null( $args->image_id ):
				$image_id = $args->image_id;
				break;
			case ! is_null( $args->term_id ):
				$image_id = get_term_meta( $args->term_id, 'thumbnail_id', true );
				break;
			default:
				$image_id = get_post_thumbnail_id( $args->post_id );
		}

		$picture_tag         = "<picture" . ( $args->picture_classes ? " class='$args->picture_classes'" : '' ) . ">";
		$args->image_classes .= $args->is_carousel && $args->current_item >= $args->start_to_deffer ? 'defer' : '';

		// Check if image should be deffered
		// Open image tag
		$img_html = "<img src='' ";
		$img_html .= $args->current_item >= $args->start_to_deffer ? ( $args->current_item > 0 && $args->is_carousel ? "loading='lazy' data-src='" : "loading='lazy' data-srcset='" ) : "srcset='";
		// Loop through array of sizes
		if ( is_array( $args->image_sizes ) && ! empty( $args->image_sizes ) ) {
			// Check if there are more than one image size provided
			$one_size = count( $args->image_sizes ) === 1;

			foreach ( $args->image_sizes as $size ) {
				$image = wp_get_attachment_image_src( $image_id, $size ) ? : wp_get_attachment_image_src( $image_id, 'full' );
				$image_path = self::imageExists( $image[0] );

				if ( ! $image_path ) {
					continue;
				}

				// Compose array of image => size for webp function
				$images[ $image[ 0 ] ] = ' ' . $image[ 1 ] . 'w, ';

				$img_html .= $image[ 0 ];

				// Get correct image size
				$image_size = getimagesize( $image_path );

				if ( ! $one_size ) {
					$img_html .= ' ' . $image[ 1 ] . 'w, ';
				}

				// Get largest image size for width and height attributes
				$image_width  = $image_size[ 0 ] > $image_width ? $image_size[ 0 ] : $image_width;
				$image_height = $image_size[ 1 ] > $image_height ? $image_size[ 1 ] : $image_height;

				$added_image_sizes = true;
			}

			// If there is no real image, check for image placeholder
			if ( ! $added_image_sizes ) {
				return $args->placeholder ? self::getAcfImage( [
					'acf_field'       => get_field( 'placeholder', 'option' ),
					'image_sizes'     => $args->image_sizes,
					'start_to_deffer' => $args->start_to_deffer,
					'current_item'    => $args->current_item,
					'picture_classes' => $args->picture_classes,
					'image_classes'   => $args->image_classes,
					'media_queries'   => $args->media_queries,
					'is_carousel'     => $args->is_carousel
				] ) : false;
			}

			// If there are not multiple sizes then add original image size
		} else {
			$image = wp_get_attachment_image_src( $image_id, $args->image_sizes );

			// If there is no real image, check for image placeholder
			if ( ! $image || ! self::imageExists( $image[ 0 ] ) ) {
				return $args->placeholder ? self::getAcfImage( [
					'acf_field'       => get_field( 'placeholder', 'option' ),
					'start_to_deffer' => $args->start_to_deffer,
					'current_item'    => $args->current_item,
					'picture_classes' => $args->picture_classes,
					'image_classes'   => $args->image_classes,
					'is_carousel'     => $args->is_carousel
				] ) : false;
			}

			$image_width  = $image[ 1 ];
			$image_height = $image[ 2 ];
			$images     = [ $image[ 0 ] => '' ];
			$img_html     .= $image[ 0 ];
		}

		// Close the srcset attribute
		$img_html .= "'";

		// Add image attributes if present any
		if ( $image_width && $image_height ) {
			$img_html .= " width='$image_width' height='$image_height'";
		}

		$img_html .= self::composeMediaQueries( $args->media_queries );

		// Apply image classes if provided
		if ( $args->image_classes ) {
			$img_html .= " class='$args->image_classes'";
		}

		// Get image alt attribute if it has any, if not then get post title
		$image_id  = get_post_thumbnail_id( $args->post_id );
		$image_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true ) ? : get_the_title( $args->post_id );
		$img_html  .= " alt='$image_alt'>";

		$webp_html = self::getWebpImage( $images, $args->media_queries, $args->current_item, $args->start_to_deffer, $args->is_carousel );

		return $picture_tag . $webp_html . $img_html . '</picture>';
	}

	/**
	 * This functions composes full image html, along with srcset and defferd srcset if needed.
	 * Returns HTML code {string} => <img src="" srcset="image_url 300w, image_url_2 450w"
	 * sizes="(min-width: 992px) 300px, (max-width: 991px) and (min-width: 768px) 450px" alt="image_alt or post_title">
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public static function getAcfImage( array $args = [] ) {
		$image_width  = null;
		$image_height = null;

		$defaults = [
			'start_to_deffer' => 0,     // Number
			'current_item'    => 0,     // Number
			'picture_classes' => '',    // String with classes
			'image_classes'   => '',    // String with classes
			'acf_field'       => null,  // Acf field array if has sizes or URL (string) if doesn't have sizes
			'image_sizes'     => '',    // ['size_one', 'size_two']
			'media_queries'   => [],    // ['(min-width: 992px)' => '500px']
			'has_sizes'       => true,  // If acf field has no sizes, then pass false so it retrieves url from image
			'is_carousel'     => false, // Check for carousel for correct deffering of the images
			'placeholder'     => true   // If true, function will look for image placeholder from global ACF Field get_field('placeholder', 'option')
		];
		$args     = (object) wp_parse_args( $args, $defaults );

		$images    = [];
		$webp_html = null;

		if ( ! $args->acf_field && ! $args->placeholder ) {
			return false;
		} else if ( ! $args->acf_field && $args->placeholder ) {
			$args->acf_field = get_field( 'placeholder', 'option' );

			// Check if placeholder exists
			if ( ! $args->acf_field ) {
				return false;
			}
		}

		$picture_tag         = "<picture" . ( $args->picture_classes ? " class='$args->picture_classes'" : '' ) . ">";
		$args->image_classes .= $args->is_carousel && $args->current_item >= $args->start_to_deffer ? 'defer' : '';

		// Check if image should be deffered
		// Open image tag
		$img_html = "<img src='' ";
		$img_html .= $args->current_item >= $args->start_to_deffer ? ( $args->current_item > 0 && $args->is_carousel ? "loading='lazy' data-src='" : "loading='lazy' data-srcset='" ) : "srcset='";

		if ( is_array( $args->image_sizes ) ) {
			$image_sizes = self::addAcfImageSizes( $args->image_sizes, $args->acf_field[ 'sizes' ] ) ? :
				// If there are no image with provided sizes, then get image from acf url field
				self::addAcfImageSizes( [ 'url' ], $args->acf_field );

			if ( ! $image_sizes ) {
				return false;
			}

			$img_html     .= $image_sizes[ 'html' ];
			$images       = $image_sizes[ 'images' ];
			$image_width  = $image_sizes[ 'image_size' ][ 0 ];
			$image_height = $image_sizes[ 'image_size' ][ 1 ];

		} else {
			$image      = $args->has_sizes ? $args->acf_field[ 'url' ] : $args->acf_field;
			$image_path = self::imageExists( $image );

			if ( ! $image_path ) {
				return false;
			}

			$image_size = getimagesize( $image_path );

			if ( $image_size ) {
				$image_width  = $image_size[ 0 ];
				$image_height = $image_size[ 1 ];
			}

			$images = [ $image => '' ];
			$img_html .= $image;
		}

		// Close the srcset attribute
		$img_html .= "'";

		// Add image attributes if present any
		if ( $image_width && $image_height ) {
			$img_html .= " width='$image_width' height='$image_height'";
		}

		$img_html .= self::composeMediaQueries( $args->media_queries );

		// Apply image classes if provided
		if ( $args->image_classes ) {
			$img_html .= " class='$args->image_classes'";
		}

		// Get image alt attribute if it has any, if not then get post title
		$image_alt = '';
		if ( $args->has_sizes ) {
			$image_alt = $args->acf_field[ 'alt' ] ? : ( $args->acf_field[ 'title' ] ? : $args->acf_field[ 'filename' ] );
		}
		$img_html .= " alt='$image_alt'>";

		$webp_html = self::getWebpImage( $images, $args->media_queries, $args->current_item, $args->start_to_deffer, $args->is_carousel );

		return $picture_tag . $webp_html . $img_html . '</picture>';
	}

	/**
	 * Image to get webp image type from jpeg/png provided images.
	 *
	 * @param array $images associative array of image_src => image_width
	 * @param array $media_queries
	 * @param int   $current_item
	 * @param int   $start_to_deffer
	 *
	 * @param bool  $is_carousel
	 *
	 * @return string
	 */
	public static function getWebpImage( array $images, array $media_queries = [], int $current_item = 0, int $start_to_deffer = 0, $is_carousel = false ): ?string {
		$counter       = 0;
		$webp          = '';
		$media_queries = array_keys( $media_queries );

		foreach ( $images as $image => $size ):
			// Get webp image name
			$webp_image = preg_replace( '/\.(jpe?g|png)$/i', '.webp', $image );
//			$webp_image = $image . '.webp';

			if ( ! $webp_image ) {
				return '';
			}

			// Get webp image absolute path on server
			$webp_image_abs = ABSPATH . wp_make_link_relative( $webp_image );

			clearstatcache();
			// If image exists add source to picture tag
			if ( file_exists( $webp_image_abs ) ):
				$src  = $current_item >= $start_to_deffer ? ( $current_item !== 1 && $is_carousel ? 'data-src' : 'data-srcset' ) : 'srcset';
				$webp .= '<source ' .
				         ( ! empty( $media_queries ) ? 'media="' . $media_queries[ $counter ] . '"' : '' ) .
				         $src . "='$webp_image' type='image/webp' class='" . ( $current_item >= $start_to_deffer && $is_carousel ? 'defer' : '' ) . "'>";
			endif;
			$counter ++;
		endforeach;

		return $webp;
	}

	/**
	 * Check if file actually exists in uploads directory
	 *
	 * @param $image
	 *
	 * @return bool
	 */
	private static function imageExists( $image ) {
		if ( ! $image ) {
			return false;
		}

		$absolute_image_path = ABSPATH . wp_make_link_relative( $image );

		return file_exists( $absolute_image_path ) ? $absolute_image_path : false;
	}

	/**
	 * @param $image_sizes
	 * @param $acf_sizes
	 *
	 * @return array | bool
	 */
	private static function addAcfImageSizes( $image_sizes, $acf_sizes ) {
		$image_width  = null;
		$image_height = null;
		$img_html     = '';
		$images       = [];

		// Loop through array of sizes
		foreach ( $image_sizes as $size ) {
			// Check if image size exists
			if ( ! array_key_exists( $size, $acf_sizes ) ) {
				continue;
			}

			$image      = $acf_sizes[ $size ];
			$image_path = self::imageExists( $image );

			// Check if image exists on server
			if ( ! $image_path ) {
				continue;
			}

			$image_size = getimagesize( $image_path );
			// Compose array of image => size for webp function
			$images[ $image ] = ' ' . $image_size[ 0 ] . 'w, ';
			$img_html         .= $image . ' ' . $image_size[ 0 ] . 'w, ';

			// Get largest image size for width and height attributes
			$image_width  = $image_size[ 0 ] > $image_width ? $image_size[ 0 ] : $image_width;
			$image_height = $image_size[ 1 ] > $image_height ? $image_size[ 0 ] : $image_height;
		}

		if ( ! $img_html ) {
			return false;
		}

		return [
			'html'       => $img_html,
			'image_size' => [ $image_width, $image_height ],
			'images'     => $images
		];
	}

	/**
	 * @param $media_queries ['(min-width: 992px)' => '500px']
	 *
	 * @return string
	 */
	private static function composeMediaQueries( $media_queries ): string {
		$html = '';
		// Check if there are media queries
		if ( empty( $media_queries ) ) {
			return $html;
		}
		// Open sizes attribute
		$html .= 'sizes="';

		foreach ( $media_queries as $media_query => $size ) {
			$sizes[] = $media_query;
			$html    .= "$media_query $size, ";
		}
		// Close sizes attribute
		$html .= '"';

		return $html;
	}
}

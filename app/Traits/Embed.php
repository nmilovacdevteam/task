<?php

namespace App\Traits;

trait Embed {
	/**
	 * Function to compose youtube iframe. Video is not loaded until user clicks on play button, only image of the video is loaded.
	 * Iframe is surrounded with div, that makes iframe responsive based on video aspect ratio provided, default is 16:9.
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public static function youtubeVideo( $args = [] ): string {
		$styles = 'img{max-width:100%;height:auto;width:100%;}div{position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);width:90px;}
					div svg path {transition:300ms all ease;}div:hover svg path:first-of-type{fill:#ff0000;}';
		$icon   = '<svg viewBox="0 0 68 48" width="100%" height="100%"><path d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74
                    C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24
                    S67.94,13.05,66.52,7.74z" fill="#212121" fill-opacity="0.8" /><path d="M 45,24 27,14 27,34" fill="#fff" fill-opacity="0.7" /></svg>';

		$defaults = [
			'url'       => '',      // Share url of the video
			'ratio'     => '16:9',  // Video ratio
			'autoplay'  => true,    // Should video play write after load
			'deffer'    => true,    // Show image of the video first instead of video
			'styles'    => $styles, // css styles for inserted document
			'play_icon' => $icon,   // svg code
			'image'     => null       // Custom image
		];
		$args     = (object) wp_parse_args( $args, $defaults );

		// Get video url and id
		$video     = self::composeUrl( $args->url );
		$video_id  = $video->id;
		$video_url = $args->autoplay ? ( $video->url . '?autoplay=1' ) : $video->url;

		// Get video thumbnail
		$image = $args->image ? : self::composeImages( $video_id );

		// Create responsive class based on provided aspect ratio, default is 16:9
		$embed_class = '';
		switch ( $args->ratio ) {
			case '16:9':
				$embed_class = 'embed-16by9';
				break;
			case '21:9':
				$embed_class = 'embed-21by9';
				break;
			case '4:3':
				$embed_class = 'embed-4by3';
				break;
			case '1:1':
				$embed_class = 'embed-1by1';
				break;
		}

		return "<div class='iframe-container $embed_class'><iframe title='Youtube Video' " .
			( $args->deffer ? " src='' srcdoc='<style>body{overflow:hidden;margin:0;}$styles</style><a href=$video_url>$image<div>$icon</div></a>'" : "src='$video_url'" ) .
			" allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></div>";
	}

	private static function composeUrl( $url ): object {
		$video_link = explode( '/', $url );
		$video_id   = array_pop( $video_link );

		return (object) [
			'url' => "https://www.youtube.com/embed/$video_id",
			'id'  => $video_id
		];
	}

	private static function composeImages( $video_id ): string {
		$resolutions = [
			'maxresdefault'
		];
		$image       = '<img src="" srcset="';

		foreach ( $resolutions as $resolution ) {
			$image .= "https://img.youtube.com/vi/$video_id/$resolution.jpg, ";
		}

		$image .= '" alt="Youtube video image">';

		return $image;
	}
}

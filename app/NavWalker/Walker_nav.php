<?php

class Walker_nav extends Walker_Nav_Menu {

	public int $megaMenuID;

	public int     $count;
	private string $callback;

	public function __construct( $callback = '' ) {
		$this->megaMenuID = 0;
		$this->callback   = $callback;
		$this->count      = 0;
	}

	public function start_lvl( &$output, $depth = 0, $args = [] ) {
		if ( $this->megaMenuID != 0 && $depth == 0 ) {
//            $output .= "\n<div class=\"mega-menu sub-menu depth_$depth\" >\n";
//            $output .= "<div class=\"mega-menu__column\"><ul>\n";

			$output .= "<div class=\"mega-menu__column\">\n";
		} else {
			$output .= "\n<ul class=\"sub-menu depth_$depth\" >\n";
		}

	}

	public function end_lvl( &$output, $depth = 0, $args = [] ) {
		if ( $this->megaMenuID != 0 && $depth == 0 ) {
//            $output .= "</ul></div>";
//            $output .= "</div>";

			$output .= "</div>";
		} else {

			$output .= "</ul>";
		}
	}

	public function start_el( &$output, $item, $depth = 0, $args = [], $id = 0 ) {

		$hasMegaMenu = get_post_meta( $item->ID, 'menu-item-mm-megamenu', true );
//        $hasColumnDivider = get_post_meta( $item->ID, 'menu-item-mm-column-divider', true );
//        $rowDivider       = get_post_meta( $item->ID, 'menu-item-mm-row-divider', true );
		$acfField = get_post_meta( $item->ID, 'menu-item-mm-acf-field', true );

		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$li_attributes = '';
		$class_names   = $value = '';

		// Soil
		$classes = empty( $item->classes ) ? [] : (array) $item->classes;
		// Remove most core classes
		$classes = preg_replace( '/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', '', $classes );
		$classes = preg_replace( '/^((menu|page)[-_\w+]+)+/', '', $classes );

		// Re-add core `menu-item` class
		$classes[] = 'menu-item';

		if ( $this->megaMenuID != 0 && $this->megaMenuID != intval( $item->menu_item_parent ) && $depth == 0 ) {
			$this->megaMenuID = 0;
		}

//        if ( $hasColumnDivider ) {
//            array_push( $classes, 'column-divider' );
//            $output .= "</ul></div><div class=\"mega-menu__column\"><ul>\n";
//        }

		// managing divider: add divider class to an element to get a divider before it.
//        if ( $rowDivider ) {
//            $output .= "<div class='fullwidth-column'></div>\n";
//        }

		if ( $hasMegaMenu ) {
			array_push( $classes, 'menu-item--mega-menu' );
			$this->megaMenuID = $item->ID;
		}

		$classes[] = ( $args->has_children ) ? 'menu-item-has-children menu-item--dropdown' : '';
		$classes[] = ( $item->current || $item->current_item_ancestor ) ? 'menu-item--active' : '';
//        $classes[] = 'menu-item-' . $item->ID;

		if ( $depth && $args->has_children ) {
			$classes[] = 'dropdown-submenu';
		}

		$class_names = ' class="' . implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) ) . '"';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		if ( $this->megaMenuID != 0 && $depth == 0 ) {
			$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . ' data-menu="#megamenu-' . $item->ID . '">';
		} else {
			$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';
		}

		$attributes = !empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= !empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
		$attributes .= !empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
		$attributes .= !empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';
//        $attributes .= ( $args->has_children ) ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';
		$attributes .= ' class="nav__link ' . ( $args->has_children ? 'nav__link--dropdown' : '' ) . ( ( $item->current || $item->current_item_ancestor ) ? 'nav__link--active' : '' ) . '"';

		$item_output = $args->before;
		$item_output .= '<a' . $attributes . '>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

		// End of the element
//        $item_output .= ( ( $depth == 0 || 1 ) && $args->has_children ) ? ' <b class="caret"></b></a>' : '</a>';
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

		if ( $acfField && $this->callback ) {
			$output .= call_user_func( $this->callback, $item );
		}
	}

	public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
		if ( !$element ) {
			return;
		}

		$id_field = $this->db_fields[ 'id' ];

		//display this element
		if ( is_array( $args[ 0 ] ) ) {
			$args[ 0 ][ 'has_children' ] = !empty( $children_elements[ $element->$id_field ] );
		} else if ( is_object( $args[ 0 ] ) ) {
			$args[ 0 ]->has_children = !empty( $children_elements[ $element->$id_field ] );
		}

		$cb_args = array_merge( [ &$output, $element, $depth ], $args );
		call_user_func_array( [ &$this, 'start_el' ], $cb_args );

		$id = $element->$id_field;

		// descend only when the depth is right and there are childrens for this element
		if ( ( $max_depth == 0 || $max_depth > $depth + 1 ) && isset( $children_elements[ $id ] ) ) {
			foreach ( $children_elements[ $id ] as $child ) {
				if ( !isset( $newlevel ) ) {
					$newlevel = true;
					//start the child delimiter
					$cb_args = array_merge( [ &$output, $depth ], $args );
					call_user_func_array( [ &$this, 'start_lvl' ], $cb_args );
				}
				$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
			}
			unset( $children_elements[ $id ] );
		}

		if ( isset( $newlevel ) && $newlevel ) {
			//end the child delimiter
			$cb_args = array_merge( [ &$output, $depth ], $args );
			call_user_func_array( [ &$this, 'end_lvl' ], $cb_args );
		}

		//end this element
		$cb_args = array_merge( [ &$output, $element, $depth ], $args );
		call_user_func_array( [ &$this, 'end_el' ], $cb_args );
	}
}

<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use Walker_nav;

class App extends Controller {
	public static function title(): string {
		$home  = get_option( 'page_for_posts', true );
		$title = '';

		switch ( true ) {
			case is_home() && $home:
				$title = get_the_title( $home );
				break;
			case is_post_type_archive():
				$title = post_type_archive_title( '', false );
				break;
			case is_archive():
				$title = single_term_title( '', false );
				break;
			case is_search():
				$title = sprintf( __( 'Rezultati pretrage za: %s', THEME_TEXT_DOMAIN ), get_search_query() );
				break;
			case is_404():
				$title = __( 'Stranica nije pronađena', THEME_TEXT_DOMAIN );
				break;
			default:
				$title = get_the_title();
		}

		return $title;
	}

	private function navigation( $menu_name, $id, $container_classes = 'navbar-collapse' ) {
		if ( !has_nav_menu( $menu_name ) ) {
			return false;
		}

		return (object) [
			'nav'     => '<div class="' . $container_classes . '" id="' . $id . '">' .
				wp_nav_menu( [ 'theme_location' => $menu_name, 'menu_class' => 'navbar-nav', 'echo' => false, 'walker' => new Walker_nav() ] ) . '</div>',
			'toggler' => '<button type="button" class="toggler toggler--navbar" data-togle="#' . $id . '" aria-controls="#' . $id . '" aria-expanded="false">
                            <span></span><span></span><span></span></button>'
		];
	}

	public function primaryNavigation() {
		return $this->navigation( 'primary_navigation', 'main-navigation' );
	}

	public function logo(): string {
		$logo = get_field( 'logo', 'option' );
		if ( !$logo ) {
			return "<a href='" . site_url( '/' ) . "' class='site-logo'>" . get_option( 'blogname' ) . "</a>";
		}

		return "<a href='" . site_url( '/' ) . "' class='site-logo'><img src='' srcset='" . $logo . "' alt=''></a>";
	}

	public function logoDark(): string {
		$logo = get_field( 'logo_dark', 'option' );
		if ( !$logo ) {
			return "<a href='" . site_url( '/' ) . "' class='site-logo'>" . get_option( 'blogname' ) . "</a>";
		}

		return "<a href='" . site_url( '/' ) . "' class='site-logo logo--dark'><img src='' srcset='" . $logo . "' alt=''></a>";
	}

	public static function closeBtn( $targetElement, $classes = '' ): string {
		return '<button type="button" class="btn btn--close ' . $classes . '" data-close="' . $targetElement . '" aria-controls="' . $targetElement . '" aria-expanded="false">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" width="20" height="20">
				<path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 
				189.28 75.93 89.21 c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 
				32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28
		        12.28-32.19 0-44.48L242.72 256z" /></svg></button>';
	}

	public static function toTop( $classes = '' ): string {
		return "<button type='button' class='btn btn--top $classes'>
				<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 448 512' width='20' height='20'>
				<path fill='currentColor' d='M34.9 289.5l-22.2-22.2c-9.4-9.4-9.4-24.6 0-33.9L207 39c9.4-9.4 24.6-9.4 33.9 0l194.3 194.3c9.4 9.4 9.4 24.6 0 33.9L413 289.4
				c-9.5 9.5-25 9.3-34.3-.4L264 168.6V456c0 13.3-10.7 24-24 24h-32c-13.3 0-24-10.7-24-24V168.6L69.2 289.1c-9.3 9.8-24.8 10-34.3.4z' /></svg></button>";
	}
}

<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Specifikacija extends Controller {

	public function links() {
		$links = get_field('sekcije');

		return $links;
	}
}

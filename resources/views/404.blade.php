@extends('layouts.app')

@section('content')
	<section class="container" style="display: grid; align-content: center; justify-items: center; margin-bottom: 60px; min-height: 50vh; text-align: center;">
		<p class="alert" style="font-size: calc(20px + (60 - 20) * ((100vw - 320px) / (1920 - 320))); margin-bottom: 30px;">404</p>
		<h1 style="margin-bottom: 50px;">{!! \App\Controllers\App::title() !!}</h1>
		<a href="{{ home_url('/') }}" class="btn btn--border">{!! __('Vrati se na početnu', THEME_TEXT_DOMAIN) !!}</a>
	</section>
@endsection

@if($socials)
	<div class="socials {{ $classes ?? '' }}">
		@foreach($socials as $url => $svg)
			@continue(!$url)
			<a href="{{ $url }}" target="_blank" rel="nofollow noopener">
				{!! $svg !!}
			</a>
		@endforeach
	</div>
@endif
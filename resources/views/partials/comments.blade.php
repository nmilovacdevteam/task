@php
	if (post_password_required()) {
	  return;
	}
	 if(!comments_open()) {
    	return;
    }
@endphp

<section id="comments" class="comments">
	@if (have_comments())
		<h2>
			{!! sprintf(_nx('One response to &ldquo;%2$s&rdquo;', '%1$s responses to &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', THEME_TEXT_DOMAIN), number_format_i18n(get_comments_number
			()), '<span>' . get_the_title() . '</span>') !!}
		</h2>

		<div class="comment-list">
			<?php wp_list_comments( [ 'style' => 'div', 'type' => 'comment', 'callback' => 'comment_template' ] ); ?>
		</div>

		@if (get_comment_pages_count() > 1 && get_option('page_comments'))
			<nav>
				<ul class="pager">
					@if (get_previous_comments_link())
						<li class="previous">@php previous_comments_link(__('&larr; Older comments', THEME_TEXT_DOMAIN)) @endphp</li>
					@endif
					@if (get_next_comments_link())
						<li class="next">@php next_comments_link(__('Newer comments &rarr;', THEME_TEXT_DOMAIN)) @endphp</li>
					@endif
				</ul>
			</nav>
		@endif
	@endif

	@if (!comments_open() && get_comments_number() != '0' && post_type_supports(get_post_type(), 'comments'))
		<div class="alert alert-warning">
			{{ __('Comments are closed.', THEME_TEXT_DOMAIN) }}
		</div>
	@endif

	<form id="comment-form">
		<div class="form-group">
			<label for="name" class="sr-only">{!! __('Ime i prezime* obavezno', THEME_TEXT_DOMAIN) !!}</label>
			<input class="form-control" id="name" name="name" type="text" required placeholder="{!! __('Ime i prezime*', THEME_TEXT_DOMAIN) !!}">
		</div>
		<div class="form-group">
			<label for="email" class="sr-only">{!! __('Email* obavezno', 'agromedia') !!}</label>
			<input class="form-control" id="email" name="email" type="email" required placeholder="{!! __('Email*', THEME_TEXT_DOMAIN) !!}">
		</div>
		<div class="form-group">
			<label for="comment" class="sr-only">{!! __('Komentar* obavezno', 'agromedia') !!}</label>
			<textarea class="form-control" id="comment" name="comment" required placeholder="{!! __('Komentar*', THEME_TEXT_DOMAIN) !!}"></textarea>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn--blue">{!! __('Dodaj komentar', THEME_TEXT_DOMAIN) !!}</button>
			<input type="hidden" name="post_id" id="postId" value="{{ get_the_ID() }}" />
		</div>
		<div class="form-notice"></div>
	</form>
</section>
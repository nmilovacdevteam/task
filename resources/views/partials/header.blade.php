<header id="header" class="scroll_nav">
    <nav class="navbar primary_navigation">
        {!! $logo !!}
        {!! $primary_navigation ? $primary_navigation->toggler : '' !!}
        {!! $primary_navigation ? $primary_navigation->nav : '' !!}
        <div class="login_wrapper">
            <svg xmlns="http://www.w3.org/2000/svg" width="326" height="50" viewBox="0 0 326 50">
                <rect id="Rectangle_152" data-name="Rectangle 152" width="326" height="50" rx="25" fill="#fcf9f7"/>
            </svg>
        </div>
    </nav>
</header>

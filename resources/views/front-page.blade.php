@extends('layouts.app')

@section('content')
    @while (have_posts()) @php the_post() @endphp
    <section class="first_fold">
        <div class="content_position">
            <h1>The home of digital skills in Belgium</h1>
            <div class="content">
                <p>Discover your perfect training match or explore our ambitious digital initiatives!</p>
            </div>
            <div class="form_search">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="835" height="96" viewBox="0 0 835 96">
                    <defs>
                        <filter id="Rectangle_3" x="0" y="0" width="835" height="96" filterUnits="userSpaceOnUse">
                            <feOffset input="SourceAlpha"/>
                            <feGaussianBlur stdDeviation="6" result="blur"/>
                            <feFlood flood-color="#1c1e21" flood-opacity="0.122"/>
                            <feComposite operator="in" in2="blur"/>
                            <feComposite in="SourceGraphic"/>
                        </filter>
                    </defs>
                    <g id="Group_137" data-name="Group 137" transform="translate(-597 -494)">
                        <g transform="matrix(1, 0, 0, 1, 597, 494)" filter="url(#Rectangle_3)">
                            <rect id="Rectangle_3-2" data-name="Rectangle 3" width="799" height="60" rx="30" transform="translate(18 18)" fill="#fff"/>
                        </g>
                        <line id="Line_1" data-name="Line 1" y2="30" transform="translate(945 527.5)" fill="none" stroke="#cbccce" stroke-width="1"/>
                        <line id="Line_4" data-name="Line 4" y2="30" transform="translate(1215 527.5)" fill="none" stroke="#cbccce" stroke-width="1"/>
                        <g id="Group_9" data-name="Group 9" transform="translate(19 1)">
                            <text id="_Search_topics" data-name=" Search topics" transform="translate(641 547)" fill="#2d2e33" font-size="16" font-family="Urbanist-Regular, Urbanist"><tspan x="0" y="0" xml:space="preserve"> Search topics</tspan></text>
                            <g id="Icon_feather-search" data-name="Icon feather-search" transform="translate(611.5 529.5)">
                                <path id="Path_1" data-name="Path 1" d="M17.758,11.129A6.629,6.629,0,1,1,11.129,4.5a6.629,6.629,0,0,1,6.629,6.629Z" fill="none" stroke="#2d2e33" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                <path id="Path_2" data-name="Path 2" d="M28.763,28.763l-3.788-3.788" transform="translate(-9.263 -9.263)" fill="none" stroke="#2d2e33" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                            </g>
                        </g>
                        <rect id="Rectangle_4" data-name="Rectangle 4" width="174" height="50" rx="25" transform="translate(1235 517)" fill="#050835"/>
                        <text id="Start_my_search" data-name="Start my search" transform="translate(1265 547)" fill="#fff" font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500"><tspan x="0" y="0">Start my search</tspan></text>
                        <text id="Search_project" data-name="Search project" transform="translate(996 547)" fill="#2d2e33" font-size="16" font-family="Urbanist-Regular, Urbanist"><tspan x="0" y="0">Search project</tspan></text>
                        <path id="portfolio" d="M20.389,2.533H14.807V1.9A1.876,1.876,0,0,0,12.961,0H8.039A1.876,1.876,0,0,0,6.193,1.9v.633H.615A.626.626,0,0,0,0,3.167V17.1A1.876,1.876,0,0,0,1.846,19H19.154A1.876,1.876,0,0,0,21,17.1V3.178A.606.606,0,0,0,20.389,2.533ZM7.424,1.9a.625.625,0,0,1,.615-.633h4.922a.625.625,0,0,1,.615.633v.633H7.424ZM19.531,3.8,17.621,9.7a.616.616,0,0,1-.584.433H13.576V9.5a.624.624,0,0,0-.615-.633H8.039a.624.624,0,0,0-.615.633v.633H3.963A.616.616,0,0,1,3.379,9.7L1.469,3.8Zm-7.185,6.333V11.4H8.654V10.133ZM19.77,17.1a.625.625,0,0,1-.615.633H1.846A.625.625,0,0,1,1.23,17.1V7.069L2.212,10.1a1.848,1.848,0,0,0,1.751,1.3H7.424v.633a.624.624,0,0,0,.615.633h4.922a.624.624,0,0,0,.615-.633V11.4h3.461a1.848,1.848,0,0,0,1.751-1.3l.982-3.031Zm0,0" transform="translate(965 532)" fill="#2d2e33"/>
                        <path id="Icon_awesome-chevron-down" data-name="Icon awesome-chevron-down" d="M3.626,13.551.51,9.884a.511.511,0,0,1,0-.64l.363-.428a.345.345,0,0,1,.543,0L3.9,11.721,6.378,8.815a.345.345,0,0,1,.543,0l.363.428a.511.511,0,0,1,0,.64L4.17,13.551A.345.345,0,0,1,3.626,13.551Z" transform="translate(1187.602 531.317)" fill="#2d2e33"/>
                    </g>
                </svg>
            <div class="content">
                <p>Or enjoy a little browsing!</p>
            </div>
            </div>
        </div>
        <div class="overlay"></div>
        <img src="@asset('images/first.png')" alt="">
        <div class="svg_position left">
            <svg xmlns="http://www.w3.org/2000/svg" width="367.758" height="341.003" viewBox="0 0 367.758 341.003">
                <g id="Group_139" data-name="Group 139" transform="translate(0)" opacity="0.356">
                    <path id="Subtraction_12" data-name="Subtraction 12" d="M176.776,0V0h0L0,173.09,176.776,341V0h0V0Zm-9.518,317.292h0L14.637,172.323,167.258,22.887v294.4Z" transform="translate(176.776 341.003) rotate(180)" fill="#fff"/>
                    <path id="Subtraction_13" data-name="Subtraction 13" d="M169,0H0L162.834,155h169L169,0Z" transform="translate(367.758 156) rotate(180)" fill="#fff"/>
                    <path id="Subtraction_14" data-name="Subtraction 14" d="M169,161H0L169.138,0h169L169,161Z" transform="translate(367.758 341.003) rotate(180)" fill="#fff"/>
                </g>
            </svg>
        </div>
        <div class="svg_position right">
            <svg xmlns="http://www.w3.org/2000/svg" width="305" height="283" viewBox="0 0 305 283">
                <g id="Group_138" data-name="Group 138" transform="translate(-1624.018 365)" opacity="0.356">
                    <path id="Subtraction_4" data-name="Subtraction 4" d="M146.982,283v0h0L0,139.352,146.982,0V283h0ZM139.068,19.678h0L12.17,139.988l126.9,124.017V19.678Z" transform="translate(1782.036 -365)" fill="#fff"/>
                    <path id="Subtraction_8" data-name="Subtraction 8" d="M140.121,128.876H0L135.009,0H275.131L140.121,128.876ZM138.86,7.483,19.647,121.339H136.121L255.334,7.483Z" transform="translate(1624.018 -364.169)" fill="#fff"/>
                    <path id="Subtraction_11" data-name="Subtraction 11" d="M140.121,0H0L135.009,128.876H275.131L140.121,0ZM138.86,121.393,19.647,7.537H136.121L255.334,121.393Z" transform="translate(1624.018 -210.877)" fill="#fff"/>
                </g>
            </svg>
        </div>
    </section>
    <section class="links">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <svg xmlns="http://www.w3.org/2000/svg" width="29" height="14" viewBox="0 0 29 14">
                            <g id="link_1_" data-name="link (1)" transform="translate(0 -106.667)">
                                <g id="Group_2" data-name="Group 2" transform="translate(0 106.667)">
                                    <g id="Group_1" data-name="Group 1" transform="translate(0 0)">
                                        <path id="Path_3" data-name="Path 3"
                                              d="M2.85,113.667a4.507,4.507,0,0,1,4.65-4.34h6v-2.66h-6a7.017,7.017,0,1,0,0,14h6v-2.66h-6A4.507,4.507,0,0,1,2.85,113.667Z"
                                              transform="translate(0 -106.667)" fill="#2d2e33"/>
                                        <rect id="Rectangle_6" data-name="Rectangle 6" width="11.999" height="3"
                                              transform="translate(8.5 5.5)" fill="#2d2e33"/>
                                        <path id="Path_4" data-name="Path 4"
                                              d="M240.667,106.667h-6v2.66h6a4.35,4.35,0,1,1,0,8.68h-6v2.66h6a7.017,7.017,0,1,0,0-14Z"
                                              transform="translate(-219.166 -106.667)" fill="#2d2e33"/>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <span>{{ __('Quick Links', THEME_TEXT_DOMAIN) }}</span>
                    </div>
                </div>
            </div>
            <div class="row quick-links-group">
                <div class="col-lg-3 single-link">
                    <div class="wrapper">
                        <div class="image_wrapper">
                            <img src="@asset('images/job.png')" alt="">
                        </div>
                        <div class="content_wrapper">
                            <p class="title">Perfect Training Match</p>
                            <p class="content">Take the test & find your training</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 single-link">
                    <div class="wrapper">
                        <div class="image_wrapper">
                            <img src="@asset('images/job.png')" alt="">
                        </div>
                        <div class="content_wrapper">
                            <p class="title">Perfect Training Match</p>
                            <p class="content">Take the test & find your training</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 single-link">
                    <div class="wrapper">
                        <div class="image_wrapper">
                            <img src="@asset('images/job.png')" alt="">
                        </div>
                        <div class="content_wrapper">
                            <p class="title">Perfect Training Match</p>
                            <p class="content">Take the test & find your training</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 single-link">
                    <div class="wrapper">
                        <div class="image_wrapper">
                            <img src="@asset('images/job.png')" alt="">
                        </div>
                        <div class="content_wrapper">
                            <p class="title">Perfect Training Match</p>
                            <p class="content">Take the test & find your training</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 single-link">
                    <div class="wrapper">
                        <div class="image_wrapper">
                            <img src="@asset('images/job.png')" alt="">
                        </div>
                        <div class="content_wrapper">
                            <p class="title">Perfect Training Match</p>
                            <p class="content">Take the test & find your training</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 single-link">
                    <div class="wrapper">
                        <div class="image_wrapper">
                            <img src="@asset('images/job.png')" alt="">
                        </div>
                        <div class="content_wrapper">
                            <p class="title">Perfect Training Match</p>
                            <p class="content">Take the test & find your training</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 single-link">
                    <div class="wrapper">
                        <div class="image_wrapper">
                            <img src="@asset('images/job.png')" alt="">
                        </div>
                        <div class="content_wrapper">
                            <p class="title">Perfect Training Match</p>
                            <p class="content">Take the test & find your training</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 single-link">
                    <div class="wrapper">
                        <div class="image_wrapper">
                            <img src="@asset('images/job.png')" alt="">
                        </div>
                        <div class="content_wrapper">
                            <p class="title">Perfect Training Match</p>
                            <p class="content">Take the test & find your training</p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <svg xmlns="http://www.w3.org/2000/svg" width="33" height="32" viewBox="0 0 33 32">
                            <g id="favorites" transform="translate(0 -3.81)">
                                <g id="Group_34" data-name="Group 34" transform="translate(1.916 3.81)">
                                    <g id="Group_33" data-name="Group 33">
                                        <path id="Path_66" data-name="Path 66"
                                              d="M58.085,16.323a2.1,2.1,0,0,0,.55-2.189,2.151,2.151,0,0,0-1.756-1.453l-7.216-1.029a.434.434,0,0,1-.327-.234L46.109,5a2.193,2.193,0,0,0-3.9,0l-3.227,6.419a.435.435,0,0,1-.328.234l-7.216,1.029a2.151,2.151,0,0,0-1.756,1.453,2.1,2.1,0,0,0,.55,2.189l5.221,5a.422.422,0,0,1,.125.378l-1.233,7.055a2.1,2.1,0,0,0,.865,2.088A2.2,2.2,0,0,0,37.5,31l6.454-3.331a.442.442,0,0,1,.4,0L50.816,31a2.2,2.2,0,0,0,2.29-.163,2.1,2.1,0,0,0,.865-2.088L52.738,21.7a.422.422,0,0,1,.125-.378Zm-7.061,5.663,1.233,7.055a.413.413,0,0,1-.173.418.431.431,0,0,1-.458.032L45.171,26.16a2.211,2.211,0,0,0-2.024,0l-6.454,3.331a.43.43,0,0,1-.458-.032.413.413,0,0,1-.173-.418l1.233-7.055a2.11,2.11,0,0,0-.625-1.89l-5.221-5a.41.41,0,0,1-.11-.438.421.421,0,0,1,.351-.291L38.9,13.342a2.172,2.172,0,0,0,1.638-1.168l3.227-6.419a.439.439,0,0,1,.78,0h0l3.227,6.419a2.172,2.172,0,0,0,1.637,1.168l7.216,1.029a.421.421,0,0,1,.351.291.41.41,0,0,1-.11.438l-5.221,5A2.11,2.11,0,0,0,51.024,21.986Z"
                                              transform="translate(-29.574 -3.81)" fill="#2d2e33"/>
                                    </g>
                                </g>
                                <g id="Group_36" data-name="Group 36" transform="translate(24.353 4.862)">
                                    <g id="Group_35" data-name="Group 35">
                                        <path id="Path_67" data-name="Path 67"
                                              d="M380.318,20.212a.875.875,0,0,0-1.222.194l-.929,1.278a.875.875,0,0,0,1.416,1.028l.929-1.278A.875.875,0,0,0,380.318,20.212Z"
                                              transform="translate(-378.001 -20.045)" fill="#2d2e33"/>
                                    </g>
                                </g>
                                <g id="Group_38" data-name="Group 38" transform="translate(5.965 4.857)">
                                    <g id="Group_37" data-name="Group 37" transform="translate(0 0)">
                                        <path id="Path_68" data-name="Path 68"
                                              d="M95.1,21.605l-.929-1.278a.875.875,0,1,0-1.416,1.028l.929,1.278A.875.875,0,1,0,95.1,21.605Z"
                                              transform="translate(-92.59 -19.966)" fill="#2d2e33"/>
                                    </g>
                                </g>
                                <g id="Group_40" data-name="Group 40" transform="translate(0 22.414)">
                                    <g id="Group_39" data-name="Group 39" transform="translate(0)">
                                        <path id="Path_69" data-name="Path 69"
                                              d="M3.21,298.1a.875.875,0,0,0-1.1-.562l-1.5.488a.875.875,0,1,0,.541,1.664l1.5-.488A.875.875,0,0,0,3.21,298.1Z"
                                              transform="translate(0 -297.493)" fill="#2d2e33"/>
                                    </g>
                                </g>
                                <g id="Group_42" data-name="Group 42" transform="translate(15.626 32.48)">
                                    <g id="Group_41" data-name="Group 41">
                                        <path id="Path_70" data-name="Path 70"
                                              d="M243.374,456.786a.875.875,0,0,0-.875.875v1.58a.875.875,0,1,0,1.75,0v-1.58A.875.875,0,0,0,243.374,456.786Z"
                                              transform="translate(-242.499 -456.786)" fill="#2d2e33"/>
                                    </g>
                                </g>
                                <g id="Group_44" data-name="Group 44" transform="translate(29.747 22.415)">
                                    <g id="Group_43" data-name="Group 43">
                                        <path id="Path_71" data-name="Path 71"
                                              d="M464.431,298.044l-1.5-.488a.875.875,0,0,0-.541,1.664l1.5.488a.875.875,0,0,0,.541-1.664Z"
                                              transform="translate(-461.783 -297.513)" fill="#2d2e33"/>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <span>{{ __('Top picks', THEME_TEXT_DOMAIN) }}</span>
                    </div>
                </div>
            </div>
            <div class="row top-picks-group">
                <div class="col-lg-3">
                    <div class="image_wrapper_top_picks">
                        <img src="@asset('images/pexels-photo-3761196.png')" alt="">
                        <div class="category">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                <path id="Icon_material-slow-motion-video" data-name="Icon material-slow-motion-video"
                                      d="M13,10.076,10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2ZM11.15,4.9V3.075a8.965,8.965,0,0,0-4.8,2L7.631,6.367A7.155,7.155,0,0,1,11.15,4.9ZM6.359,7.643,5.069,6.358A9.011,9.011,0,0,0,3.075,11.17H4.9A7.193,7.193,0,0,1,6.359,7.643ZM4.9,12.98H3.075a9.011,9.011,0,0,0,1.994,4.812L6.359,16.5A7.126,7.126,0,0,1,4.9,12.98Zm1.453,6.1a8.994,8.994,0,0,0,4.8,2V19.248a7.155,7.155,0,0,1-3.519-1.465L6.35,19.076Zm14.725-7a9.05,9.05,0,0,1-8.075,9V19.248A7.238,7.238,0,0,0,13,4.9V3.075A9.05,9.05,0,0,1,21.075,12.075Z"
                                      transform="translate(-3.075 -3.075)" fill="#fff"/>
                            </svg>
                            <span> {{ __('Video', THEME_TEXT_DOMAIN) }}</span>
                        </div>
                    </div>
                    <div class="title_top_picks">
                        <h3>{{ __('Training course W',THEME_TEXT_DOMAIN) }}</h3>
                        <div class="content_top_picks">
                            <p>“Because of this 2-day training, I feel more secure in my role & my future at work.”</p>
                        </div>
                        <a class="view_more" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                                <g id="Group_90" data-name="Group 90" transform="translate(-285 -2503)">
                                    <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                          font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                        <tspan x="0" y="0">View more</tspan>
                                    </text>
                                    <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                          d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                          transform="translate(371 2505.353)"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="image_wrapper_top_picks">
                        <img src="@asset('images/pexels-photo-3761196.png')" alt="">
                        <div class="category">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                <path id="Icon_material-slow-motion-video" data-name="Icon material-slow-motion-video"
                                      d="M13,10.076,10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2ZM11.15,4.9V3.075a8.965,8.965,0,0,0-4.8,2L7.631,6.367A7.155,7.155,0,0,1,11.15,4.9ZM6.359,7.643,5.069,6.358A9.011,9.011,0,0,0,3.075,11.17H4.9A7.193,7.193,0,0,1,6.359,7.643ZM4.9,12.98H3.075a9.011,9.011,0,0,0,1.994,4.812L6.359,16.5A7.126,7.126,0,0,1,4.9,12.98Zm1.453,6.1a8.994,8.994,0,0,0,4.8,2V19.248a7.155,7.155,0,0,1-3.519-1.465L6.35,19.076Zm14.725-7a9.05,9.05,0,0,1-8.075,9V19.248A7.238,7.238,0,0,0,13,4.9V3.075A9.05,9.05,0,0,1,21.075,12.075Z"
                                      transform="translate(-3.075 -3.075)" fill="#fff"/>
                            </svg>
                            <span> {{ __('Video', THEME_TEXT_DOMAIN) }}</span>
                        </div>
                    </div>
                    <div class="title_top_picks">
                        <h3>{{ __('Training course W',THEME_TEXT_DOMAIN) }}</h3>
                        <div class="content_top_picks">
                            <p>“Because of this 2-day training, I feel more secure in my role & my future at work.”</p>
                        </div>
                        <a class="view_more" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                                <g id="Group_90" data-name="Group 90" transform="translate(-285 -2503)">
                                    <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                          font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                        <tspan x="0" y="0">View more</tspan>
                                    </text>
                                    <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                          d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                          transform="translate(371 2505.353)"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="image_wrapper_top_picks">
                        <img src="@asset('images/pexels-photo-3761196.png')" alt="">
                        <div class="category">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                <path id="Icon_material-slow-motion-video" data-name="Icon material-slow-motion-video"
                                      d="M13,10.076,10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2ZM11.15,4.9V3.075a8.965,8.965,0,0,0-4.8,2L7.631,6.367A7.155,7.155,0,0,1,11.15,4.9ZM6.359,7.643,5.069,6.358A9.011,9.011,0,0,0,3.075,11.17H4.9A7.193,7.193,0,0,1,6.359,7.643ZM4.9,12.98H3.075a9.011,9.011,0,0,0,1.994,4.812L6.359,16.5A7.126,7.126,0,0,1,4.9,12.98Zm1.453,6.1a8.994,8.994,0,0,0,4.8,2V19.248a7.155,7.155,0,0,1-3.519-1.465L6.35,19.076Zm14.725-7a9.05,9.05,0,0,1-8.075,9V19.248A7.238,7.238,0,0,0,13,4.9V3.075A9.05,9.05,0,0,1,21.075,12.075Z"
                                      transform="translate(-3.075 -3.075)" fill="#fff"/>
                            </svg>
                            <span> {{ __('Video', THEME_TEXT_DOMAIN) }}</span>
                        </div>
                    </div>
                    <div class="title_top_picks">
                        <h3>{{ __('Training course W',THEME_TEXT_DOMAIN) }}</h3>
                        <div class="content_top_picks">
                            <p>“Because of this 2-day training, I feel more secure in my role & my future at work.”</p>
                        </div>
                        <a class="view_more" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                                <g id="Group_90" data-name="Group 90" transform="translate(-285 -2503)">
                                    <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                          font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                        <tspan x="0" y="0">View more</tspan>
                                    </text>
                                    <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                          d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                          transform="translate(371 2505.353)"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="image_wrapper_top_picks">
                        <img src="@asset('images/pexels-photo-3761196.png')" alt="">
                        <div class="category">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                <path id="Icon_material-slow-motion-video" data-name="Icon material-slow-motion-video"
                                      d="M13,10.076,10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2ZM11.15,4.9V3.075a8.965,8.965,0,0,0-4.8,2L7.631,6.367A7.155,7.155,0,0,1,11.15,4.9ZM6.359,7.643,5.069,6.358A9.011,9.011,0,0,0,3.075,11.17H4.9A7.193,7.193,0,0,1,6.359,7.643ZM4.9,12.98H3.075a9.011,9.011,0,0,0,1.994,4.812L6.359,16.5A7.126,7.126,0,0,1,4.9,12.98Zm1.453,6.1a8.994,8.994,0,0,0,4.8,2V19.248a7.155,7.155,0,0,1-3.519-1.465L6.35,19.076Zm14.725-7a9.05,9.05,0,0,1-8.075,9V19.248A7.238,7.238,0,0,0,13,4.9V3.075A9.05,9.05,0,0,1,21.075,12.075Z"
                                      transform="translate(-3.075 -3.075)" fill="#fff"/>
                            </svg>
                            <span> {{ __('Video', THEME_TEXT_DOMAIN) }}</span>
                        </div>
                    </div>
                    <div class="title_top_picks">
                        <h3>{{ __('Training course W',THEME_TEXT_DOMAIN) }}</h3>
                        <div class="content_top_picks">
                            <p>“Because of this 2-day training, I feel more secure in my role & my future at work.”</p>
                        </div>
                        <a class="view_more" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                                <g id="Group_90" data-name="Group 90" transform="translate(-285 -2503)">
                                    <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                          font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                        <tspan x="0" y="0">View more</tspan>
                                    </text>
                                    <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                          d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                          transform="translate(371 2505.353)"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="initiatives">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>
                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                            <g id="rocket" transform="translate(0 -0.049)">
                                <path id="Path_5" data-name="Path 5"
                                      d="M31.061.05A25.3,25.3,0,0,0,13.021,7.8L11.331,9.492l-1.8-1.8-5.306,1.5L0,13.421l3.7,3.7h0l.191.191L2.152,20.907,3.3,22.055.492,24.862l1.326,1.326,2.807-2.808,1.358,1.358L.038,30.684l1.326,1.326,5.944-5.945,1.358,1.358L5.859,30.231l1.326,1.326,2.807-2.808L11.14,29.9l3.592-1.741,3.893,3.893,4.227-4.228,1.5-5.3-1.8-1.8,1.691-1.691a25.755,25.755,0,0,0,5.62-8.061A25,25,0,0,0,32,.049ZM5.208,10.864,8.985,9.8,10,10.818,5.027,15.8,2.652,13.421Zm6.315,16.763-7.1-7.1L5.3,18.718l8.032,8.033ZM22.25,23.067,21.181,26.84,18.624,29.4l-2.375-2.376,4.977-4.978Zm.685-5.386-1.709,1.71h0l-6.3,6.3-8.57-8.571,6.3-6.3L14.365,9.11A23.465,23.465,0,0,1,30.1,1.947a23.419,23.419,0,0,1-7.167,15.733Z"
                                      fill="#fcf9f7"/>
                                <path id="Path_6" data-name="Path 6"
                                      d="M258.117,131.635a4.274,4.274,0,1,0-6.043,0,4.283,4.283,0,0,0,6.043,0Zm-3.019-5.424a2.4,2.4,0,1,1-2.4,2.4C252.7,127.967,253.785,126.211,255.1,126.211Z"
                                      transform="translate(-235.145 -116.515)" fill="#fcf9f7"/>
                            </g>
                        </svg>
                        {{ __('Our most ambitious initiatives',THEME_TEXT_DOMAIN) }}
                    </h2>
                </div>
            </div>
            <div class="row group-initiatives">
                <div class="col-lg-4 single-initiatives">
                    <img src="@asset('images/initiatives.png')" alt="">
                    <h3>{{ __('Hack Your Future',THEME_TEXT_DOMAIN) }}</h3>
                    <a href="#">
                        <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                            <g id="Group_87" data-name="Group 87" transform="translate(-285 -2503)">
                                <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                      fill="#fcf9f7" font-size="16" font-family="Urbanist-Medium, Urbanist"
                                      font-weight="500">
                                    <tspan x="0" y="0">View more</tspan>
                                </text>
                                <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                      d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                      transform="translate(371 2505.353)" fill="#fcf9f7"/>
                            </g>
                        </svg>
                    </a>
                </div>
                <div class="col-lg-4 single-initiatives">
                    <img src="@asset('images/initiatives.png')" alt="">
                    <h3>{{ __('Hack Your Future',THEME_TEXT_DOMAIN) }}</h3>
                    <a href="#">
                        <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                            <g id="Group_87" data-name="Group 87" transform="translate(-285 -2503)">
                                <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                      fill="#fcf9f7" font-size="16" font-family="Urbanist-Medium, Urbanist"
                                      font-weight="500">
                                    <tspan x="0" y="0">View more</tspan>
                                </text>
                                <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                      d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                      transform="translate(371 2505.353)" fill="#fcf9f7"/>
                            </g>
                        </svg>
                    </a>
                </div>
                <div class="col-lg-4 single-initiatives">
                    <img src="@asset('images/initiatives.png')" alt="">
                    <h3>{{ __('Hack Your Future',THEME_TEXT_DOMAIN) }}</h3>
                    <a href="#">
                        <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                            <g id="Group_87" data-name="Group 87" transform="translate(-285 -2503)">
                                <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                      fill="#fcf9f7" font-size="16" font-family="Urbanist-Medium, Urbanist"
                                      font-weight="500">
                                    <tspan x="0" y="0">View more</tspan>
                                </text>
                                <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                      d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                      transform="translate(371 2505.353)" fill="#fcf9f7"/>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="highlight">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="image">
                        <div class="overlay"></div>
                        <div class="overlay_content">
                            <h2>
                                <svg xmlns="http://www.w3.org/2000/svg" width="34" height="32" viewBox="0 0 34 32">
                                    <g id="promotion" transform="translate(0 0)">
                                        <path id="Path_72" data-name="Path 72"
                                              d="M122.954,290.808a.66.66,0,1,0-.9-.242A.661.661,0,0,0,122.954,290.808Zm0,0"
                                              transform="translate(-113.862 -270.449)" fill="#fff"/>
                                        <path id="Path_73" data-name="Path 73"
                                              d="M6.34,45.8l3.326,5.761a2.642,2.642,0,1,0,4.576-2.642L12.26,45.483l1.716-.991a.661.661,0,0,0,.242-.9L13.359,42.1,26.01,39.621a1.981,1.981,0,0,0,1.606-2.969l-2.2-3.8,1.4-2.125a.661.661,0,0,0,.021-.694l-1.321-2.288a.662.662,0,0,0-.612-.329l-2.541.152-2.437-4.221a1.964,1.964,0,0,0-1.691-.99h-.025a1.964,1.964,0,0,0-1.668.914L7.921,34.261,2.312,37.5A4.623,4.623,0,0,0,6.34,45.8ZM13.1,49.577A1.321,1.321,0,0,1,10.81,50.9l-3.3-5.721L9.8,43.856Zm-1.5-5.237-.66-1.144,1.144-.66.66,1.144ZM24.584,28.758l.913,1.581-.814,1.233L23.11,28.846Zm-6.93-4.764a.66.66,0,0,1,1.136.013l7.682,13.307a.66.66,0,0,1-.556.99,2.426,2.426,0,0,0-.362.061L17.428,24.289C17.6,24.07,17.621,24.049,17.654,23.993Zm-1.105,1.416,7.635,13.224-11.523,2.26-3.5-6.069ZM1.764,43.154a3.3,3.3,0,0,1,1.209-4.511l5.148-2.972,3.3,5.72L6.275,44.363A3.306,3.306,0,0,1,1.764,43.154Zm0,0"
                                              transform="translate(0 -20.879)" fill="#fff"/>
                                        <path id="Path_74" data-name="Path 74"
                                              d="M46.863,309.9a.661.661,0,0,0-.9-.242l-1.144.66a.661.661,0,0,1-.9-.242.66.66,0,0,0-1.144.661,1.984,1.984,0,0,0,2.707.725l1.144-.66A.661.661,0,0,0,46.863,309.9Zm0,0"
                                              transform="translate(-39.863 -289.126)" fill="#fff"/>
                                        <path id="Path_75" data-name="Path 75"
                                              d="M440.295,42.565l-3.68,2.048a.661.661,0,0,0,.642,1.154l3.68-2.048a.66.66,0,0,0-.642-1.154Zm0,0"
                                              transform="translate(-407.276 -39.675)" fill="#fff"/>
                                        <path id="Path_76" data-name="Path 76"
                                              d="M443.447,137.328l-2.552-.684a.66.66,0,1,0-.342,1.276l2.552.684a.66.66,0,1,0,.342-1.276Zm0,0"
                                              transform="translate(-410.814 -127.597)" fill="#fff"/>
                                        <path id="Path_77" data-name="Path 77"
                                              d="M384.2.49l-.684,2.552a.661.661,0,0,0,1.276.342l.684-2.552A.66.66,0,1,0,384.2.49Zm0,0"
                                              transform="translate(-358.017)" fill="#fff"/>
                                    </g>
                                </svg>
                                <span> {{ __('This week’s fantastical highlight', THEME_TEXT_DOMAIN) }}</span>
                            </h2>

                            <div class="content">
                                <p>Mauris augue neque gravida in. Amet consectetur adipiscing elit ut aliquam purus sit
                                    amet. Lectus mauris ultrices eros in cursus. Vel quam elementum pulvinar etiam non
                                    quam lacus suspendisse. </p>
                                <p>Turpis massa sed elementum tempus. Pellentesque massa
                                    placerat duis ultricies lacus. Tincidunt nunc pulvinar sapien et. Scelerisque
                                    eleifend donec pretium vulputate sapien nec sagittis. Semper feugiat nibh sed
                                    pulvinar proin. Consectetur libero id faucibus nisl tincidunt. Nunc id cursus metus
                                    aliquam.</p>
                            </div>
                            <a href="#" class="learn_more">
                                <svg xmlns="http://www.w3.org/2000/svg" width="200" height="40" viewBox="0 0 200 40">
                                    <g id="Group_110" data-name="Group 110" transform="translate(-322 -1893)">
                                        <rect id="Rectangle_69" data-name="Rectangle 69" width="200" height="40" rx="20"
                                              transform="translate(322 1893)" fill="#fff"/>
                                        <text id="I_want_to_know_more" data-name="I want to know more"
                                              transform="translate(352 1918)" fill="#2d2e33" font-size="16"
                                              font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                            <tspan x="0" y="0">I want to know more</tspan>
                                        </text>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <div class="sponsored">
                            <svg xmlns="http://www.w3.org/2000/svg" width="87" height="30" viewBox="0 0 87 30">
                                <g id="Group_111" data-name="Group 111" transform="translate(-322 -1893)"
                                   opacity="0.397" style="mix-blend-mode: normal;isolation: isolate">
                                    <rect id="Rectangle_69" data-name="Rectangle 69" width="87" height="30" rx="5"
                                          transform="translate(322 1893)" fill="#fff"/>
                                    <text id="Sponsored" transform="translate(332 1913)" fill="#2d2e33"
                                          font-size="14" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                        <tspan x="0" y="0">Sponsored</tspan>
                                    </text>
                                </g>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="links testemonials">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="31" viewBox="0 0 34 31">
                            <g id="quotation" transform="translate(-0.001)">
                                <path id="Path_78" data-name="Path 78"
                                      d="M33.91,22.018a15.8,15.8,0,0,1-.584-7.166A12.156,12.156,0,0,0,25.112.572a.5.5,0,0,0-.626.321.494.494,0,0,0,.324.621,11.17,11.17,0,0,1,7.542,13.131l0,.022a16.882,16.882,0,0,0,.619,7.668.682.682,0,0,1-.125.668.716.716,0,0,1-.652.253,8.024,8.024,0,0,1-2.7-.811,3.819,3.819,0,0,0-3.378-.132,11.306,11.306,0,0,1-12.729-2.28,11.1,11.1,0,0,1-.089-15.658A11.232,11.232,0,0,1,21.143.991a11.621,11.621,0,0,1,1.745.1.495.495,0,1,0,.131-.98A12.672,12.672,0,0,0,21.123,0a12.279,12.279,0,0,0-10.8,6.876A12.158,12.158,0,0,0,.675,21.517a15.789,15.789,0,0,1-.584,7.166,1.667,1.667,0,0,0,.3,1.614,1.71,1.71,0,0,0,1.554.6,9.016,9.016,0,0,0,3.04-.92,2.811,2.811,0,0,1,2.487-.106A13.556,13.556,0,0,0,12.86,31a11.872,11.872,0,0,0,5.557-1.444,12.418,12.418,0,0,0,4.189-3.662.492.492,0,0,0-.115-.69.5.5,0,0,0-.695.114,11.419,11.419,0,0,1-3.852,3.367,11.316,11.316,0,0,1-10.052.292,3.818,3.818,0,0,0-3.378.132,8.022,8.022,0,0,1-2.7.811.717.717,0,0,1-.652-.253A.681.681,0,0,1,1.035,29a16.871,16.871,0,0,0,.619-7.667l0-.023a11.159,11.159,0,0,1,8.2-13.315,12.09,12.09,0,0,0,2.827,12.735,11.983,11.983,0,0,0,8.468,3.606,12.67,12.67,0,0,0,5.388-1.126,2.812,2.812,0,0,1,2.487.106,9.014,9.014,0,0,0,3.04.92,1.713,1.713,0,0,0,1.554-.6A1.667,1.667,0,0,0,33.91,22.018Zm0,0"
                                      transform="translate(0 0)" fill="#2d2e33"/>
                                <path id="Path_79" data-name="Path 79"
                                      d="M219.9,128.16h-1.571a1.861,1.861,0,0,0-1.859,1.859V131.9a1.861,1.861,0,0,0,1.861,1.859h0a1.357,1.357,0,0,1,1.156.738,3.674,3.674,0,0,1,.35,1.192,1.145,1.145,0,0,0,1.232,1.09,1.217,1.217,0,0,0,1.054-.608,7.391,7.391,0,0,0,.552-5.9A2.882,2.882,0,0,0,219.9,128.16Zm1.365,7.5a.227.227,0,0,1-.253.112.221.221,0,0,1-.184-.2,4.668,4.668,0,0,0-.445-1.515,2.369,2.369,0,0,0-2.052-1.3h0a.862.862,0,0,1-.861-.861v-1.879a.862.862,0,0,1,.861-.861H219.9a1.882,1.882,0,0,1,1.814,1.374A6.429,6.429,0,0,1,221.267,135.66Zm0,0"
                                      transform="translate(-202.1 -119.74)" fill="#2d2e33"/>
                                <path id="Path_80" data-name="Path 80"
                                      d="M328.391,130.019V131.9a1.861,1.861,0,0,0,1.857,1.859,1.366,1.366,0,0,1,1.16.738,3.668,3.668,0,0,1,.35,1.192,1.175,1.175,0,0,0,1.232,1.09,1.218,1.218,0,0,0,1.054-.608,7.39,7.39,0,0,0,.552-5.9,2.882,2.882,0,0,0-2.776-2.107h-1.571A1.861,1.861,0,0,0,328.391,130.019Zm1,0a.863.863,0,0,1,.862-.861h1.571a1.882,1.882,0,0,1,1.814,1.374,6.429,6.429,0,0,1-.449,5.129.228.228,0,0,1-.254.112.221.221,0,0,1-.183-.2,4.667,4.667,0,0,0-.445-1.515,2.369,2.369,0,0,0-2.052-1.3h0a.862.862,0,0,1-.862-.861Zm0,0"
                                      transform="translate(-306.588 -119.74)" fill="#2d2e33"/>
                            </g>
                        </svg>
                        <span>{{ __('Testimonials à la Belge', THEME_TEXT_DOMAIN) }}</span>
                    </div>
                </div>
            </div>
            <div class="row top-picks-group">
                <div class="col-lg-3">
                    <div class="image_wrapper_top_picks">
                        <img src="@asset('images/pexels-photo-3761196.png')" alt="">
                        <div class="category">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                <path id="Icon_material-slow-motion-video" data-name="Icon material-slow-motion-video"
                                      d="M13,10.076,10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2ZM11.15,4.9V3.075a8.965,8.965,0,0,0-4.8,2L7.631,6.367A7.155,7.155,0,0,1,11.15,4.9ZM6.359,7.643,5.069,6.358A9.011,9.011,0,0,0,3.075,11.17H4.9A7.193,7.193,0,0,1,6.359,7.643ZM4.9,12.98H3.075a9.011,9.011,0,0,0,1.994,4.812L6.359,16.5A7.126,7.126,0,0,1,4.9,12.98Zm1.453,6.1a8.994,8.994,0,0,0,4.8,2V19.248a7.155,7.155,0,0,1-3.519-1.465L6.35,19.076Zm14.725-7a9.05,9.05,0,0,1-8.075,9V19.248A7.238,7.238,0,0,0,13,4.9V3.075A9.05,9.05,0,0,1,21.075,12.075Z"
                                      transform="translate(-3.075 -3.075)" fill="#fff"/>
                            </svg>
                            <span> {{ __('Video', THEME_TEXT_DOMAIN) }}</span>
                        </div>
                    </div>
                    <div class="title_top_picks">
                        <h3>{{ __('Training course W',THEME_TEXT_DOMAIN) }}</h3>
                        <div class="content_top_picks">
                            <p>“Because of this 2-day training, I feel more secure in my role & my future at work.”</p>
                        </div>
                        <a class="view_more" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                                <g id="Group_90" data-name="Group 90" transform="translate(-285 -2503)">
                                    <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                          font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                        <tspan x="0" y="0">View more</tspan>
                                    </text>
                                    <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                          d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                          transform="translate(371 2505.353)"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="image_wrapper_top_picks">
                        <img src="@asset('images/pexels-photo-3761196.png')" alt="">
                        <div class="category">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                <path id="Icon_material-slow-motion-video" data-name="Icon material-slow-motion-video"
                                      d="M13,10.076,10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2ZM11.15,4.9V3.075a8.965,8.965,0,0,0-4.8,2L7.631,6.367A7.155,7.155,0,0,1,11.15,4.9ZM6.359,7.643,5.069,6.358A9.011,9.011,0,0,0,3.075,11.17H4.9A7.193,7.193,0,0,1,6.359,7.643ZM4.9,12.98H3.075a9.011,9.011,0,0,0,1.994,4.812L6.359,16.5A7.126,7.126,0,0,1,4.9,12.98Zm1.453,6.1a8.994,8.994,0,0,0,4.8,2V19.248a7.155,7.155,0,0,1-3.519-1.465L6.35,19.076Zm14.725-7a9.05,9.05,0,0,1-8.075,9V19.248A7.238,7.238,0,0,0,13,4.9V3.075A9.05,9.05,0,0,1,21.075,12.075Z"
                                      transform="translate(-3.075 -3.075)" fill="#fff"/>
                            </svg>
                            <span> {{ __('Video', THEME_TEXT_DOMAIN) }}</span>
                        </div>
                    </div>
                    <div class="title_top_picks">
                        <h3>{{ __('Training course W',THEME_TEXT_DOMAIN) }}</h3>
                        <div class="content_top_picks">
                            <p>“Because of this 2-day training, I feel more secure in my role & my future at work.”</p>
                        </div>
                        <a class="view_more" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                                <g id="Group_90" data-name="Group 90" transform="translate(-285 -2503)">
                                    <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                          font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                        <tspan x="0" y="0">View more</tspan>
                                    </text>
                                    <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                          d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                          transform="translate(371 2505.353)"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="image_wrapper_top_picks">
                        <img src="@asset('images/pexels-photo-3761196.png')" alt="">
                        <div class="category">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                <path id="Icon_material-slow-motion-video" data-name="Icon material-slow-motion-video"
                                      d="M13,10.076,10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2ZM11.15,4.9V3.075a8.965,8.965,0,0,0-4.8,2L7.631,6.367A7.155,7.155,0,0,1,11.15,4.9ZM6.359,7.643,5.069,6.358A9.011,9.011,0,0,0,3.075,11.17H4.9A7.193,7.193,0,0,1,6.359,7.643ZM4.9,12.98H3.075a9.011,9.011,0,0,0,1.994,4.812L6.359,16.5A7.126,7.126,0,0,1,4.9,12.98Zm1.453,6.1a8.994,8.994,0,0,0,4.8,2V19.248a7.155,7.155,0,0,1-3.519-1.465L6.35,19.076Zm14.725-7a9.05,9.05,0,0,1-8.075,9V19.248A7.238,7.238,0,0,0,13,4.9V3.075A9.05,9.05,0,0,1,21.075,12.075Z"
                                      transform="translate(-3.075 -3.075)" fill="#fff"/>
                            </svg>
                            <span> {{ __('Video', THEME_TEXT_DOMAIN) }}</span>
                        </div>
                    </div>
                    <div class="title_top_picks">
                        <h3>{{ __('Training course W',THEME_TEXT_DOMAIN) }}</h3>
                        <div class="content_top_picks">
                            <p>“Because of this 2-day training, I feel more secure in my role & my future at work.”</p>
                        </div>
                        <a class="view_more" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                                <g id="Group_90" data-name="Group 90" transform="translate(-285 -2503)">
                                    <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                          font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                        <tspan x="0" y="0">View more</tspan>
                                    </text>
                                    <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                          d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                          transform="translate(371 2505.353)"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="image_wrapper_top_picks">
                        <img src="@asset('images/pexels-photo-3761196.png')" alt="">
                        <div class="category">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                <path id="Icon_material-slow-motion-video" data-name="Icon material-slow-motion-video"
                                      d="M13,10.076,10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2ZM11.15,4.9V3.075a8.965,8.965,0,0,0-4.8,2L7.631,6.367A7.155,7.155,0,0,1,11.15,4.9ZM6.359,7.643,5.069,6.358A9.011,9.011,0,0,0,3.075,11.17H4.9A7.193,7.193,0,0,1,6.359,7.643ZM4.9,12.98H3.075a9.011,9.011,0,0,0,1.994,4.812L6.359,16.5A7.126,7.126,0,0,1,4.9,12.98Zm1.453,6.1a8.994,8.994,0,0,0,4.8,2V19.248a7.155,7.155,0,0,1-3.519-1.465L6.35,19.076Zm14.725-7a9.05,9.05,0,0,1-8.075,9V19.248A7.238,7.238,0,0,0,13,4.9V3.075A9.05,9.05,0,0,1,21.075,12.075Z"
                                      transform="translate(-3.075 -3.075)" fill="#fff"/>
                            </svg>
                            <span> {{ __('Video', THEME_TEXT_DOMAIN) }}</span>
                        </div>
                    </div>
                    <div class="title_top_picks">
                        <h3>{{ __('Training course W',THEME_TEXT_DOMAIN) }}</h3>
                        <div class="content_top_picks">
                            <p>“Because of this 2-day training, I feel more secure in my role & my future at work.”</p>
                        </div>
                        <a class="view_more" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                                <g id="Group_90" data-name="Group 90" transform="translate(-285 -2503)">
                                    <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                          font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                        <tspan x="0" y="0">View more</tspan>
                                    </text>
                                    <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                          d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                          transform="translate(371 2505.353)"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="digital">
        <div class="container">
            <div class="row col-lg-12">
                <div class="image_wrapper">
                    <div class="overlay"></div>
                    <img src="@asset('images/mask2.png')" alt="">
                    <div class="content">
                        <h2>Do you have a digital initiative that you’d like to share with Belgium and its
                            citizens?</h2>
                        <h3>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <g id="handshake_1_" data-name="handshake (1)" transform="translate(0.001 -0.5)">
                                    <path id="Path_81" data-name="Path 81"
                                          d="M23.9,105.1l-4.017-5.289a.466.466,0,0,0-.625-.111l-2.594,1.672H13.283a.461.461,0,0,0-.129.022l-2.623.765L7.81,101.4,5.166,99.7a.466.466,0,0,0-.619.1L.1,105.38a.481.481,0,0,0,.059.661l2.329,2.031,2.089,2.853a1.7,1.7,0,0,0,.037,2.345,1.647,1.647,0,0,0,1.009.484,1.719,1.719,0,0,0-.019.258,1.686,1.686,0,0,0,.487,1.192,1.641,1.641,0,0,0,1.176.494h.018c0,.012,0,.023,0,.034a1.687,1.687,0,0,0,.487,1.192,1.642,1.642,0,0,0,1.176.494,1.675,1.675,0,0,0,.26-.021,1.686,1.686,0,0,0,.478,1.026,1.649,1.649,0,0,0,2.352,0l.254-.258.107.087a1.649,1.649,0,0,0,2.332-.019,1.693,1.693,0,0,0,.486-1.218,1.648,1.648,0,0,0,1.2-.493,1.694,1.694,0,0,0,.486-1.218,1.642,1.642,0,0,0,1.2-.493,1.693,1.693,0,0,0,.486-1.218,1.647,1.647,0,0,0,1.2-.493,1.705,1.705,0,0,0,.219-2.11l1.525-3.238,2.293-2A.481.481,0,0,0,23.9,105.1ZM5.015,100.733l1.9,1.225L2.78,107.066,1.123,105.62Zm.268,10.828.574-.582c.017-.018.036-.034.054-.05a.715.715,0,0,1,.392-.161h.012c.021,0,.041,0,.062,0a.713.713,0,0,1,.515.229.757.757,0,0,1-.032,1.038L6.3,112.6a.716.716,0,0,1-1.021,0A.74.74,0,0,1,5.283,111.561ZM6.76,114.53a.738.738,0,0,1,0-1.035l1.113-1.128a.722.722,0,0,1,.821-.144.731.731,0,0,1,.4.755.776.776,0,0,1-.215.441l-1.1,1.11a.715.715,0,0,1-1.021,0Zm1.68,1.719a.738.738,0,0,1,0-1.035l1.113-1.128a.723.723,0,0,1,.873-.116.731.731,0,0,1,.345.739.777.777,0,0,1-.214.429l-1.095,1.11a.715.715,0,0,1-1.021,0Zm1.914,1.5a.739.739,0,0,1,0-1.035l.574-.582c.017-.018.036-.034.054-.05h0a.715.715,0,0,1,.32-.151h.008c.02,0,.041-.007.061-.009H11.4l.051,0a.713.713,0,0,1,.515.229.657.657,0,0,1,.047.055.763.763,0,0,1-.081.985l-.554.561A.716.716,0,0,1,10.354,117.748Zm8.779-5.323a.716.716,0,0,1-1.021,0,.429.429,0,0,0-.032-.029l-3.026-3.1a.466.466,0,0,0-.666,0,.481.481,0,0,0,0,.674l3.059,3.132h0a.74.74,0,0,1,0,1.035.715.715,0,0,1-1.021,0l-2.046-2.1a.466.466,0,0,0-.665,0,.482.482,0,0,0,0,.675l2.015,2.063a.45.45,0,0,0,.033.037.74.74,0,0,1,0,1.035.716.716,0,0,1-1.021,0L13.618,114.7a.466.466,0,0,0-.666,0,.481.481,0,0,0,0,.674l1.086,1.112a.422.422,0,0,0,.033.037.74.74,0,0,1,0,1.035.716.716,0,0,1-1.021,0,1.043,1.043,0,0,0-.107-.086c-.031-.024-.039-.04-.023-.078s.039-.073.054-.112.031-.087.045-.132a1.742,1.742,0,0,0,.061-.734,1.682,1.682,0,0,0-.7-1.161,1.639,1.639,0,0,0-.7-.275,1.717,1.717,0,0,0-.4-1.535,1.641,1.641,0,0,0-1.185-.527h-.05a1.7,1.7,0,0,0-.445-1.192,1.641,1.641,0,0,0-1.185-.527,1.67,1.67,0,0,0-.4.043,1.679,1.679,0,0,0-.55-1.022,1.637,1.637,0,0,0-1.064-.409,1.654,1.654,0,0,0-1.136.433L3.435,107.76l4.352-5.374,1.387.385-1.811,3.24a1.694,1.694,0,0,0-.156,1.28,1.671,1.671,0,0,0,.783,1.017l.073.04.006,0a1.66,1.66,0,0,0,.2.085,1.631,1.631,0,0,0,.985.031,1.656,1.656,0,0,0,1-.794l1.177-2.105,2.1.151,5.6,5.672A.74.74,0,0,1,19.133,112.426Zm.2-2.186-5.251-5.321a.419.419,0,0,0-.031-.029.467.467,0,0,0-.268-.109l-2.574-.185a.477.477,0,0,0-.443.241L9.438,107.2a.719.719,0,0,1-.436.344.709.709,0,0,1-.548-.069.726.726,0,0,1-.34-.441.735.735,0,0,1,.068-.556l1.767-3.16.716-.209.014,0,2.672-.782h3.229l4.031,5.193Zm1.9-3.47-3.727-4.8,1.9-1.226,3.483,4.585Zm0,0"
                                          transform="translate(0 -94.416)" fill="#fff"/>
                                    <path id="Path_82" data-name="Path 82"
                                          d="M300.621,33.083a.476.476,0,0,0,.337-.14l3.258-3.258a.477.477,0,0,0-.675-.675l-3.258,3.258a.477.477,0,0,0,.337.814Zm0,0"
                                          transform="translate(-286.116 -27.023)" fill="#fff"/>
                                    <path id="Path_83" data-name="Path 83"
                                          d="M126.593,32.943a.477.477,0,1,0,.675-.675l-3.258-3.258a.477.477,0,0,0-.675.675Zm0,0"
                                          transform="translate(-117.437 -27.023)" fill="#fff"/>
                                    <path id="Path_84" data-name="Path 84"
                                          d="M246.438,44.349a.477.477,0,0,0,.477-.477v-2.7a.477.477,0,0,0-.954,0v2.7A.477.477,0,0,0,246.438,44.349Zm0,0"
                                          transform="translate(-234.438 -38.289)" fill="#fff"/>
                                    <path id="Path_85" data-name="Path 85"
                                          d="M246.022,1.158A.477.477,0,1,0,246.2.579a.48.48,0,0,0-.176.579Zm0,0"
                                          transform="translate(-234.463 0)" fill="#fff"/>
                                </g>
                            </svg>
                            <span>Become one of our trusted partners</span></h3>
                        <div class="excerpt">
                            <p>Mauris augue neque gravida in. Amet consectetur adipiscing elit ut aliquam purus sit
                                amet.
                                Lectus mauris ultrices eros in cursus. Vel quam elementum pulvinar etiam non quam lacus
                                suspendisse.</p>
                            <p>Turpis massa sed elementum tempus. Pellentesque massa placerat duis ultricies
                                lacus. Tincidunt nunc pulvinar sapien et. Scelerisque eleifend donec pretium vulputate
                                sapien nec sagittis. Semper feugiat nibh sed pulvinar proin. Consectetur libero id
                                faucibus
                                nisl tincidunt. Nunc id cursus metus aliquam.</p>
                        </div>
                        <a class="more" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="200" height="40" viewBox="0 0 200 40">
                                <g id="Group_86" data-name="Group 86" transform="translate(-322 -1893)">
                                    <rect id="Rectangle_69" data-name="Rectangle 69" width="200" height="40" rx="20"
                                          transform="translate(322 1893)" fill="#fff"/>
                                    <text id="I_want_to_know_more" data-name="I want to know more"
                                          transform="translate(352 1918)" fill="#2d2e33" font-size="16"
                                          font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                        <tspan x="0" y="0">I want to know more</tspan>
                                    </text>
                                </g>
                            </svg>
                        </a>
                    </div>
                    <svg class="overlay_svg" xmlns="http://www.w3.org/2000/svg" width="367.758" height="341.003"
                         viewBox="0 0 367.758 341.003">
                        <g id="Group_99" data-name="Group 99" opacity="0.356">
                            <path id="Subtraction_12" data-name="Subtraction 12"
                                  d="M176.776,0V0h0L0,173.09,176.776,341V0h0V0Zm-9.518,317.292h0L14.637,172.323,167.258,22.887v294.4Z"
                                  transform="translate(176.776 341.003) rotate(180)" fill="#fff"/>
                            <path id="Subtraction_13" data-name="Subtraction 13" d="M169,0H0L162.834,155h169L169,0Z"
                                  transform="translate(367.758 156) rotate(180)" fill="#fff"/>
                            <path id="Subtraction_14" data-name="Subtraction 14" d="M169,161H0L169.138,0h169L169,161Z"
                                  transform="translate(367.758 341.003) rotate(180)" fill="#fff"/>
                        </g>
                    </svg>
                </div>
            </div>
        </div>
    </section>
    <section class="news">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="title">
                        <svg xmlns="http://www.w3.org/2000/svg" id="newspaper" width="34" height="32"
                             viewBox="0 0 34 32">
                            <path id="Path_55" data-name="Path 55"
                                  d="M32.284,0H7.408A1.733,1.733,0,0,0,5.692,1.745v2.65H1.716A1.733,1.733,0,0,0,0,6.14V28.6A3.377,3.377,0,0,0,3.32,32H20.962a.507.507,0,0,0,0-1.013H5.723A3.42,3.42,0,0,0,6.688,28.6V16.754a.5.5,0,1,0-1,0V28.6a2.371,2.371,0,0,1-2.335,2.388H3.344A2.371,2.371,0,0,1,1,28.6V6.14a.727.727,0,0,1,.72-.732H5.692v9.455a.5.5,0,1,0,1,0V1.745a.727.727,0,0,1,.72-.732H32.284a.727.727,0,0,1,.72.732V28.6a2.371,2.371,0,0,1-2.348,2.388H22.822a.507.507,0,0,0,0,1.013h7.834A3.377,3.377,0,0,0,34,28.6V1.745A1.733,1.733,0,0,0,32.284,0Zm0,0"
                                  fill="#2d2e33"/>
                            <path id="Path_56" data-name="Path 56"
                                  d="M147.773,170.25h6.947a1.2,1.2,0,0,0,1.2-1.2V162.1a1.2,1.2,0,0,0-1.2-1.2h-6.947a1.2,1.2,0,0,0-1.2,1.2v6.947A1.2,1.2,0,0,0,147.773,170.25Zm-.185-8.146a.186.186,0,0,1,.185-.185h6.947a.186.186,0,0,1,.185.185v6.947a.186.186,0,0,1-.185.185h-6.947a.186.186,0,0,1-.185-.185Zm0,0"
                                  transform="translate(-136.902 -150.039)" fill="#2d2e33"/>
                            <path id="Path_57" data-name="Path 57"
                                  d="M305.2,161.919h9.4a.507.507,0,0,0,0-1.013h-9.4a.507.507,0,1,0,0,1.013Zm0,0"
                                  transform="translate(-284.692 -150.039)" fill="#2d2e33"/>
                            <path id="Path_58" data-name="Path 58"
                                  d="M305.2,223.6h9.4a.507.507,0,0,0,0-1.013h-9.4a.507.507,0,1,0,0,1.013Zm0,0"
                                  transform="translate(-284.692 -207.553)" fill="#2d2e33"/>
                            <path id="Path_59" data-name="Path 59"
                                  d="M305.2,285.275h9.4a.507.507,0,0,0,0-1.013h-9.4a.507.507,0,1,0,0,1.013Zm0,0"
                                  transform="translate(-284.692 -265.063)" fill="#2d2e33"/>
                            <path id="Path_60" data-name="Path 60"
                                  d="M147.081,346.951h20.08a.507.507,0,0,0,0-1.013h-20.08a.507.507,0,1,0,0,1.013Zm0,0"
                                  transform="translate(-137.254 -322.574)" fill="#2d2e33"/>
                            <path id="Path_61" data-name="Path 61"
                                  d="M147.081,408.63h20.08a.507.507,0,0,0,0-1.013h-20.08a.507.507,0,1,0,0,1.013Zm0,0"
                                  transform="translate(-137.254 -380.088)" fill="#2d2e33"/>
                            <path id="Path_62" data-name="Path 62"
                                  d="M146.766,43.96V38.608a.273.273,0,0,1,.157-.251.774.774,0,0,1,.379-.087.8.8,0,0,1,.454.107,1.341,1.341,0,0,1,.346.462l1.592,3.076V38.6a.265.265,0,0,1,.157-.247.913.913,0,0,1,.759,0,.265.265,0,0,1,.157.247V43.96a.276.276,0,0,1-.161.247.748.748,0,0,1-.375.091.644.644,0,0,1-.619-.338l-1.773-3.315V43.96a.275.275,0,0,1-.161.247.829.829,0,0,1-.755,0A.276.276,0,0,1,146.766,43.96Zm0,0"
                                  transform="translate(-137.041 -35.685)" fill="#2d2e33"/>
                            <path id="Path_63" data-name="Path 63"
                                  d="M220.645,43.96V38.608a.281.281,0,0,1,.14-.247.593.593,0,0,1,.33-.091h2.936a.278.278,0,0,1,.251.14.6.6,0,0,1,.087.322.61.61,0,0,1-.091.338.281.281,0,0,1-.247.14h-2.334v1.649h1.254a.288.288,0,0,1,.247.128.507.507,0,0,1,.091.3.5.5,0,0,1-.087.285.285.285,0,0,1-.251.128h-1.254v1.658h2.334a.282.282,0,0,1,.247.14.61.61,0,0,1,.091.338.6.6,0,0,1-.087.322.278.278,0,0,1-.251.14h-2.936a.593.593,0,0,1-.33-.09A.281.281,0,0,1,220.645,43.96Zm0,0"
                                  transform="translate(-206.023 -35.685)" fill="#2d2e33"/>
                            <path id="Path_64" data-name="Path 64"
                                  d="M284.023,38.7a.361.361,0,0,1,.223-.3.915.915,0,0,1,.462-.128q.3,0,.355.214l1.3,4.47.7-2.87q.074-.3.528-.3t.52.3l.7,2.87,1.3-4.47q.058-.214.355-.214a.915.915,0,0,1,.462.128.361.361,0,0,1,.223.3.316.316,0,0,1-.016.1l-1.633,5.187q-.124.363-.7.363a1.016,1.016,0,0,1-.445-.095.4.4,0,0,1-.239-.268l-.528-2.227-.536,2.227a.4.4,0,0,1-.239.268,1.016,1.016,0,0,1-.445.095,1.036,1.036,0,0,1-.453-.095.428.428,0,0,1-.247-.268L284.04,38.8A.309.309,0,0,1,284.023,38.7Zm0,0"
                                  transform="translate(-265.246 -35.685)" fill="#2d2e33"/>
                            <path id="Path_65" data-name="Path 65"
                                  d="M393.68,42.778a.642.642,0,0,1,.144-.384.408.408,0,0,1,.326-.194.422.422,0,0,1,.243.1,2.8,2.8,0,0,1,.264.227,1.211,1.211,0,0,0,.367.227,1.321,1.321,0,0,0,.528.1,1.137,1.137,0,0,0,.66-.182.608.608,0,0,0,.264-.536.719.719,0,0,0-.144-.441,1.148,1.148,0,0,0-.379-.322q-.235-.128-.515-.247t-.565-.268a3.293,3.293,0,0,1-.519-.334,1.334,1.334,0,0,1-.379-.495,1.647,1.647,0,0,1-.144-.7,1.665,1.665,0,0,1,.177-.783,1.349,1.349,0,0,1,.474-.527,2.219,2.219,0,0,1,.635-.276,2.843,2.843,0,0,1,.718-.088,3.7,3.7,0,0,1,.454.03,4.2,4.2,0,0,1,.507.1,1.067,1.067,0,0,1,.437.212.434.434,0,0,1,.169.339.751.751,0,0,1-.115.38.351.351,0,0,1-.313.2,1.553,1.553,0,0,1-.4-.157,1.689,1.689,0,0,0-.734-.157,1.2,1.2,0,0,0-.689.169.553.553,0,0,0-.045.882,1.553,1.553,0,0,0,.491.289q.292.111.635.28a5.426,5.426,0,0,1,.635.367,1.541,1.541,0,0,1,.491.573,1.859,1.859,0,0,1,.2.879,1.621,1.621,0,0,1-.54,1.305,2.122,2.122,0,0,1-1.423.46,2.658,2.658,0,0,1-1.344-.322Q393.679,43.158,393.68,42.778Zm0,0"
                                  transform="translate(-367.671 -35.113)" fill="#2d2e33"/>
                        </svg>
                        <span>{{ __('News from the Belgian scene',THEME_TEXT_DOMAIN) }}</span>
                    </h2>
                </div>
            </div>
            <div class="row news-group">
                <div class="col-lg-3">
                    <div class="news-group-wrapper">
                        <div class="image_wrapper_news_group">
                            <img src="@asset('images/pexels-photo-3761196.png')" alt="">
                            <div class="category">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                    <path id="Icon_material-slow-motion-video"
                                          data-name="Icon material-slow-motion-video"
                                          d="M13,10.076,10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2ZM11.15,4.9V3.075a8.965,8.965,0,0,0-4.8,2L7.631,6.367A7.155,7.155,0,0,1,11.15,4.9ZM6.359,7.643,5.069,6.358A9.011,9.011,0,0,0,3.075,11.17H4.9A7.193,7.193,0,0,1,6.359,7.643ZM4.9,12.98H3.075a9.011,9.011,0,0,0,1.994,4.812L6.359,16.5A7.126,7.126,0,0,1,4.9,12.98Zm1.453,6.1a8.994,8.994,0,0,0,4.8,2V19.248a7.155,7.155,0,0,1-3.519-1.465L6.35,19.076Zm14.725-7a9.05,9.05,0,0,1-8.075,9V19.248A7.238,7.238,0,0,0,13,4.9V3.075A9.05,9.05,0,0,1,21.075,12.075Z"
                                          transform="translate(-3.075 -3.075)" fill="#fff"/>
                                </svg>
                                <span> {{ __('Video', THEME_TEXT_DOMAIN) }}</span>
                            </div>
                        </div>
                        <div class="title_top_news-group">
                            <h3>{{ __('Training course W',THEME_TEXT_DOMAIN) }}</h3>
                            <div class="content_news_group">
                                <p>“Because of this 2-day training, I feel more secure in my role & my future at
                                    work.”</p>
                            </div>
                            <a class="view_more" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                                    <g id="Group_90" data-name="Group 90" transform="translate(-285 -2503)">
                                        <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                              font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                            <tspan x="0" y="0">View more</tspan>
                                        </text>
                                        <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                              d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                              transform="translate(371 2505.353)"/>
                                    </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="news-group-wrapper">
                        <div class="image_wrapper_news_group">
                            <img src="@asset('images/pexels-photo-3761196.png')" alt="">
                            <div class="category">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                    <path id="Icon_material-slow-motion-video"
                                          data-name="Icon material-slow-motion-video"
                                          d="M13,10.076,10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2ZM11.15,4.9V3.075a8.965,8.965,0,0,0-4.8,2L7.631,6.367A7.155,7.155,0,0,1,11.15,4.9ZM6.359,7.643,5.069,6.358A9.011,9.011,0,0,0,3.075,11.17H4.9A7.193,7.193,0,0,1,6.359,7.643ZM4.9,12.98H3.075a9.011,9.011,0,0,0,1.994,4.812L6.359,16.5A7.126,7.126,0,0,1,4.9,12.98Zm1.453,6.1a8.994,8.994,0,0,0,4.8,2V19.248a7.155,7.155,0,0,1-3.519-1.465L6.35,19.076Zm14.725-7a9.05,9.05,0,0,1-8.075,9V19.248A7.238,7.238,0,0,0,13,4.9V3.075A9.05,9.05,0,0,1,21.075,12.075Z"
                                          transform="translate(-3.075 -3.075)" fill="#fff"/>
                                </svg>
                                <span> {{ __('Video', THEME_TEXT_DOMAIN) }}</span>
                            </div>
                        </div>
                        <div class="title_top_news-group">
                            <h3>{{ __('Training course W',THEME_TEXT_DOMAIN) }}</h3>
                            <div class="content_news_group">
                                <p>“Because of this 2-day training, I feel more secure in my role & my future at
                                    work.”</p>
                            </div>
                            <a class="view_more" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                                    <g id="Group_90" data-name="Group 90" transform="translate(-285 -2503)">
                                        <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                              font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                            <tspan x="0" y="0">View more</tspan>
                                        </text>
                                        <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                              d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                              transform="translate(371 2505.353)"/>
                                    </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="news-group-wrapper">
                        <div class="image_wrapper_news_group">
                            <img src="@asset('images/pexels-photo-3761196.png')" alt="">
                            <div class="category">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                    <path id="Icon_material-slow-motion-video"
                                          data-name="Icon material-slow-motion-video"
                                          d="M13,10.076,10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2ZM11.15,4.9V3.075a8.965,8.965,0,0,0-4.8,2L7.631,6.367A7.155,7.155,0,0,1,11.15,4.9ZM6.359,7.643,5.069,6.358A9.011,9.011,0,0,0,3.075,11.17H4.9A7.193,7.193,0,0,1,6.359,7.643ZM4.9,12.98H3.075a9.011,9.011,0,0,0,1.994,4.812L6.359,16.5A7.126,7.126,0,0,1,4.9,12.98Zm1.453,6.1a8.994,8.994,0,0,0,4.8,2V19.248a7.155,7.155,0,0,1-3.519-1.465L6.35,19.076Zm14.725-7a9.05,9.05,0,0,1-8.075,9V19.248A7.238,7.238,0,0,0,13,4.9V3.075A9.05,9.05,0,0,1,21.075,12.075Z"
                                          transform="translate(-3.075 -3.075)" fill="#fff"/>
                                </svg>
                                <span> {{ __('Video', THEME_TEXT_DOMAIN) }}</span>
                            </div>
                        </div>
                        <div class="title_top_news-group">
                            <h3>{{ __('Training course W',THEME_TEXT_DOMAIN) }}</h3>
                            <div class="content_news_group">
                                <p>“Because of this 2-day training, I feel more secure in my role & my future at
                                    work.”</p>
                            </div>
                            <a class="view_more" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                                    <g id="Group_90" data-name="Group 90" transform="translate(-285 -2503)">
                                        <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                              font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                            <tspan x="0" y="0">View more</tspan>
                                        </text>
                                        <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                              d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                              transform="translate(371 2505.353)"/>
                                    </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="news-group-wrapper">
                        <div class="image_wrapper_news_group">
                            <img src="@asset('images/pexels-photo-3761196.png')" alt="">
                            <div class="category">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                    <path id="Icon_material-slow-motion-video"
                                          data-name="Icon material-slow-motion-video"
                                          d="M13,10.076,10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2Zm0,0L10.248,8v8.141L13,14.074l2.662-2ZM11.15,4.9V3.075a8.965,8.965,0,0,0-4.8,2L7.631,6.367A7.155,7.155,0,0,1,11.15,4.9ZM6.359,7.643,5.069,6.358A9.011,9.011,0,0,0,3.075,11.17H4.9A7.193,7.193,0,0,1,6.359,7.643ZM4.9,12.98H3.075a9.011,9.011,0,0,0,1.994,4.812L6.359,16.5A7.126,7.126,0,0,1,4.9,12.98Zm1.453,6.1a8.994,8.994,0,0,0,4.8,2V19.248a7.155,7.155,0,0,1-3.519-1.465L6.35,19.076Zm14.725-7a9.05,9.05,0,0,1-8.075,9V19.248A7.238,7.238,0,0,0,13,4.9V3.075A9.05,9.05,0,0,1,21.075,12.075Z"
                                          transform="translate(-3.075 -3.075)" fill="#fff"/>
                                </svg>
                                <span> {{ __('Video', THEME_TEXT_DOMAIN) }}</span>
                            </div>
                        </div>
                        <div class="title_top_news-group">
                            <h3>{{ __('Training course W',THEME_TEXT_DOMAIN) }}</h3>
                            <div class="content_news_group">
                                <p>“Because of this 2-day training, I feel more secure in my role & my future at
                                    work.”</p>
                            </div>
                            <a class="view_more" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19">
                                    <g id="Group_90" data-name="Group 90" transform="translate(-285 -2503)">
                                        <text id="View_more" data-name="View more" transform="translate(285 2518)"
                                              font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500">
                                            <tspan x="0" y="0">View more</tspan>
                                        </text>
                                        <path id="Icon_awesome-arrow-right" data-name="Icon awesome-arrow-right"
                                              d="M3.827,3.25l.446-.458a.472.472,0,0,1,.681,0l3.9,4a.5.5,0,0,1,0,.7L4.953,11.5a.472.472,0,0,1-.681,0l-.446-.458a.5.5,0,0,1,.008-.707l2.42-2.366H.482A.487.487,0,0,1,0,7.477V6.818a.487.487,0,0,1,.482-.495H6.255L3.835,3.957A.5.5,0,0,1,3.827,3.25Z"
                                              transform="translate(371 2505.353)"/>
                                    </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="match">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 content_centered">
                    <div class="content_wrapper">
                        <h2 class="title">
                            <svg xmlns="http://www.w3.org/2000/svg" width="29.894" height="29.894"
                                 viewBox="0 0 29.894 29.894">
                                <path id="Icon_material-slow-motion-video" data-name="Icon material-slow-motion-video"
                                      d="M19.558,14.7l-4.57-3.44v13.52l4.57-3.44,4.42-3.32Zm0,0-4.57-3.44v13.52l4.57-3.44,4.42-3.32Zm0,0-4.57-3.44v13.52l4.57-3.44,4.42-3.32ZM16.486,6.109V3.075a14.888,14.888,0,0,0-7.972,3.32l2.128,2.148A11.884,11.884,0,0,1,16.486,6.109ZM8.529,10.661,6.387,8.528A14.965,14.965,0,0,0,3.075,16.52H6.1A11.945,11.945,0,0,1,8.529,10.661ZM6.1,19.524H3.075a14.965,14.965,0,0,0,3.312,7.992l2.143-2.148A11.835,11.835,0,0,1,6.1,19.524ZM8.514,29.649a14.937,14.937,0,0,0,7.972,3.32V29.935A11.884,11.884,0,0,1,10.642,27.5L8.514,29.649ZM32.969,18.022A15.031,15.031,0,0,1,19.558,32.969V29.935a12.02,12.02,0,0,0,0-23.825V3.075A15.031,15.031,0,0,1,32.969,18.022Z"
                                      transform="translate(-3.075 -3.075)" fill="#2d2e33"/>
                            </svg>
                            <span>{{ __('Your perfect training match', THEME_TEXT_DOMAIN) }}</span>
                        </h2>
                        <h3>Take the free myUQ Skills test to discover your talents!</h3>
                        <div class="content">
                            <p>Mauris augue neque gravida in. Amet consectetur adipiscing elit ut aliquam purus sit
                                amet.
                                Lectus mauris ultrices eros in cursus. Vel quam elementum pulvinar etiam non quam lacus
                                suspendisse. </p>
                            <p>Turpis massa sed elementum tempus. Pellentesque massa placerat duis ultricies
                                lacus. Tincidunt nunc pulvinar sapien et. Scelerisque eleifend donec pretium vulputate
                                sapien nec sagittis. Semper feugiat nibh sed pulvinar proin. Consectetur libero id
                                faucibus
                                nisl tincidunt. Nunc id cursus metus aliquam.</p>
                        </div>
                        <a href="#" class="more">
                            <svg xmlns="http://www.w3.org/2000/svg" width="153" height="40" viewBox="0 0 153 40">
                                <g id="Group_84" data-name="Group 84" transform="translate(-322 -1893)">
                                    <rect id="Rectangle_69" data-name="Rectangle 69" width="153" height="40" rx="20"
                                          transform="translate(322 1893)" fill="#050835"/>
                                    <text id="Take_the_test" data-name="Take the test" transform="translate(352 1918)"
                                          fill="#fcf9f7" font-size="16" font-family="Urbanist-Medium, Urbanist"
                                          font-weight="500">
                                        <tspan x="0" y="0">Take the test</tspan>
                                    </text>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="image_wrapper">
                        <div class="overlay">
                            <svg xmlns="http://www.w3.org/2000/svg" width="61" height="61" viewBox="0 0 61 61">
                                <path id="Icon_material-slow-motion-video" data-name="Icon material-slow-motion-video"
                                      d="M36.709,26.8l-9.326-7.02V47.369l9.326-7.02,9.02-6.774Zm0,0-9.326-7.02V47.369l9.326-7.02,9.02-6.774Zm0,0-9.326-7.02V47.369l9.326-7.02,9.02-6.774ZM30.441,9.267V3.075A30.38,30.38,0,0,0,14.174,9.849l4.342,4.383A24.249,24.249,0,0,1,30.441,9.267ZM14.2,18.555,9.832,14.2A30.537,30.537,0,0,0,3.075,30.51H9.251A24.375,24.375,0,0,1,14.2,18.555ZM9.251,36.64H3.075A30.537,30.537,0,0,0,9.832,52.948L14.2,48.564A24.15,24.15,0,0,1,9.251,36.64ZM14.174,57.3a30.479,30.479,0,0,0,16.267,6.774V57.883a24.249,24.249,0,0,1-11.925-4.966L14.174,57.3Zm49.9-23.726a30.671,30.671,0,0,1-27.366,30.5V57.883a24.528,24.528,0,0,0,0-48.616V3.075A30.671,30.671,0,0,1,64.075,33.575Z"
                                      transform="translate(-3.075 -3.075)" fill="#fff"/>
                            </svg>
                        </div>
                        <img src="@asset('images/unsplash.png')" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="questions">
        <img src="@asset('images/bg_image.png')" alt="">
        <div class="content">
            <svg xmlns="http://www.w3.org/2000/svg" width="810" height="192" viewBox="0 0 810 192">
                <g id="Group_85" data-name="Group 85" transform="translate(-855 -6080)">
                    <g id="Group_63" data-name="Group 63" transform="translate(533 4339)">
                        <g id="Rectangle_69" data-name="Rectangle 69" transform="translate(322 1893)" fill="none" stroke="#fff" stroke-width="1">
                            <rect width="150" height="40" rx="20" stroke="none"/>
                            <rect x="0.5" y="0.5" width="149" height="39" rx="19.5" fill="none"/>
                        </g>
                        <text id="Get_in_touch" data-name="Get in touch" transform="translate(352 1918)" fill="#fff" font-size="16" font-family="Urbanist-Medium, Urbanist" font-weight="500"><tspan x="0" y="0">Get in touch</tspan></text>
                    </g>
                    <text id="Ask_our_team_" data-name="Ask our team!" transform="translate(855 6160)" fill="#fff" font-size="35" font-family="Urbanist-Medium, Urbanist" font-weight="500"><tspan x="0" y="33">Ask our team!</tspan></text>
                    <text id="Do_you_have_any_questions_" data-name="Do you have any questions?" transform="translate(901 6080)" fill="#fff" font-size="50" font-family="Urbanist-Bold, Urbanist" font-weight="700"><tspan x="0" y="47">Do you have any questions?</tspan></text>
                    <path id="communication" d="M29.2,25.008a9.413,9.413,0,0,0,1.861-5.625,9.808,9.808,0,0,0-4.537-8.165A11.792,11.792,0,0,0,22.64,3.554,13.788,13.788,0,0,0,13.285,0,13.788,13.788,0,0,0,3.929,3.554,11.727,11.727,0,0,0,0,12.262,11.494,11.494,0,0,0,2.37,19.257L.088,24.9A1.213,1.213,0,0,0,1.7,26.468L8.211,23.6a13.964,13.964,0,0,0,2.52.7,10.445,10.445,0,0,0,3.67,3.574,11.484,11.484,0,0,0,5.708,1.547c.03,0,.06,0,.09,0a11.547,11.547,0,0,0,4.046-.721l5.11,2.252a1.213,1.213,0,0,0,1.614-1.565Zm-2.514.256.917,2.267L24.76,26.279a1.213,1.213,0,0,0-.966-.006A9.1,9.1,0,0,1,20.25,27L20.2,27a8.917,8.917,0,0,1-6.228-2.489,14.146,14.146,0,0,0,5.711-1.495,1.213,1.213,0,0,0-1.1-2.164,11.879,11.879,0,0,1-9.918.317,1.213,1.213,0,0,0-.966.006L3.457,23.042l1.429-3.535a1.213,1.213,0,0,0-.2-1.242,9.165,9.165,0,0,1-2.259-6c0-5.424,4.871-9.836,10.859-9.836s10.859,4.412,10.859,9.836a9.125,9.125,0,0,1-1.489,4.973,1.213,1.213,0,0,0,2.036,1.321,11.639,11.639,0,0,0,1.71-4.332,7.217,7.217,0,0,1,2.233,5.16,7.081,7.081,0,0,1-1.747,4.638A1.213,1.213,0,0,0,26.685,25.264ZM14.437,18.5a1.213,1.213,0,0,1-1.213,1.213h0a1.213,1.213,0,0,1,0-2.427h0A1.213,1.213,0,0,1,14.437,18.5Zm3.4-9.09c0,.017,0,.034,0,.051A4.545,4.545,0,0,1,15.07,13.65a.961.961,0,0,0-.572.892v.078a1.213,1.213,0,1,1-2.427,0v-.078a3.383,3.383,0,0,1,2.046-3.123A2.122,2.122,0,0,0,15.408,9.5c0-.012,0-.023,0-.035a2.123,2.123,0,0,0-4.246,0,1.213,1.213,0,1,1-2.427,0,4.549,4.549,0,0,1,9.1-.051Z" transform="translate(855.001 6093.94)" fill="#fff"/>
                </g>
            </svg>
        </div>
    </section>
    @endwhile
@endsection

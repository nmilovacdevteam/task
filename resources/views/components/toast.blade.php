<?php
if ( !isset( $slot ) ) {
	return;
}

$animation     = $animation ?? 'bottom';
$toast_id      = $toast_id ?? 'toast-' . time();
$toast_classes = $toast_classes ?? '';
$data_attr     = $data_attr ?? '';
$data_show     = $data_show ?? false;
$data_close    = $data_close ?? false;
$has_close_btn = $has_close_btn ?? true;
?>

<div class="toast toast--{{ $animation }} {{ $toast_classes }}"
     {!! $data_attr !!} {!! $data_show ? "data-show_time='$data_show'" : '' !!} {!! $data_close ? "data-close_time='$data_close'" : '' !!}
     id="{{ $toast_id }}" {!! !$has_close_btn ? "data-close='#$toast_id'" : '' !!}>
	@if($has_close_btn)
		<div class="toast__header">
			{!! \App\Controllers\App::closeBtn( '#' . $toast_id , $close_btn_classes ?? '' ) !!}
		</div>
	@endif
	<div class="toast__body">
		{!! $slot !!}
	</div>
</div>
<?php
if ( !isset( $slot ) ) {
	return;
}

$indicators = $indicators ?? true;
$controls   = $controls ?? true;
$slider_id  = $slider_id ?? 'glide-' . time();
$defaults   = [
	'type'        => 'carousel',
	'perView'     => 3,
	'autoplay'    => 3500,
	'breakpoints' => [
		991 => [
			'perView' => 2
		],
		767 => [
			'perView' => 1
		]
	],
	'peek'        => [
		'before' => 5,
		'after'  => 5
	]
];
$options    = $options ?? [];
$args       = wp_parse_args( $options, $defaults );
$arrows     = $arrows ?? [
		'left'  => 'M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z',
		'right' => 'M190.5 66.9l22.2-22.2c9.4-9.4 24.6-9.4 33.9 0L441 239c9.4 9.4 9.4 24.6 0 33.9L246.6 467.3c-9.4 9.4-24.6 9.4-33.9 0l-22.2-22.2c-9.5-9.5-9.3-25 .4-34.3L311.4 296H24c-13.3 0-24-10.7-24-24v-32c0-13.3 10.7-24 24-24h287.4L190.9 101.2c-9.8-9.3-10-24.8-.4-34.3z'
	];
?>

<div class="glide {{ $slider_classes ?? '' }}" id="{{ $slider_id }}" data-options="{{ json_encode($args) }}">
	<div class="glide__track" data-glide-el="track">
		<ul class="glide__slides">
			{!! $slot !!}
		</ul>
	</div>

	@if($controls)
		<div class="glide__arrows" data-glide-el="controls">
			<button class="glide__arrow glide__arrow--left" data-glide-dir="<">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="20" height="20">
					<path fill="currentColor" d="{{ $arrows['left'] }}" />
				</svg>
			</button>
			<button class="glide__arrow glide__arrow--right" data-glide-dir=">">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="20" height="20">
					<path fill="currentColor" d="{{ $arrows['right'] }}" />
				</svg>
			</button>
		</div>
	@endif

	@if($indicators && isset($slides) && (bool) $slides)
		<div class="glide__bullets" data-glide-el="controls[nav]">
			@for($i = 0; $i < (int) $slides; $i++)
				<button class="glide__bullet" data-glide-dir="={{ $i }}"></button>
			@endfor
		</div>
	@endif
</div>

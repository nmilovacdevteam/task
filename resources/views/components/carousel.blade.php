<?php
if ( !isset( $slot ) ) {
	return;
}

$indicators  = $indicators ?? true;
$controls    = $controls ?? true;
$carousel_id = $carousel_id ?? 'carousel-' . time();
?>
<div id="{{ $carousel_id }}" class="carousel slide {{ $carousel_classes ?? '' }}" data-ride="carousel">
	@if($indicators && isset($slides) && (bool) $slides)
		<ol class="carousel-indicators">
			@for($i = 0; $i < (int) $slides; $i++)
				<li data-target="#{{ $carousel_id }}" data-slide-to="{{ $i }}" class="{{ $i == 0 ? 'active' : '' }}"></li>
			@endfor
		</ol>
	@endif
	<div class="carousel-inner">
		{!! $slot !!}
	</div>
	@if($controls)
		<a class="carousel__control carousel-control-prev" href="#{{ $carousel_id }}" role="button" data-slide="prev">
			<span class="carousel__control__icon carousel__control__icon--prev" aria-hidden="true">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="20" height="20">
                    <path fill="currentColor" d="M193.456 357.573L127.882 292H424c13.255 0 24-10.745 24-24v-24c0-13.255-10.745-24-24-24H127.882l65.574-65.573
                    c9.373-9.373 9.373-24.569 0-33.941l-16.971-16.971c-9.373-9.373-24.569-9.373-33.941 0L7.029 239.029c-9.373 9.373-9.373 24.568 0 33.941l135.515 135.515
                    c9.373 9.373 24.569 9.373 33.941 0l16.971-16.971c9.373-9.372 9.373-24.568 0-33.941z" />
                </svg>
            </span>
			<span class="sr-only">{!! __('Prethodni', THEME_TEXT_DOMAIN) !!}</span>
		</a>
		<a class="carousel__control carousel-control-next" href="#{{ $carousel_id }}" role="button" data-slide="next">
			<span class="carousel__control__icon carousel__control__icon--next" aria-hidden="true">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="20" height="20">
                    <path fill="currentColor" d="M254.544 154.427L320.118 220H24c-13.255 0-24 10.745-24 24v24c0 13.255 10.745 24 24 24h296.118l-65.574 65.573
                    c-9.373 9.373-9.373 24.569 0 33.941l16.971 16.971c9.373 9.373 24.569 9.373 33.941 0L440.97 272.97c9.373-9.373 9.373-24.569 0-33.941L305.456 103.515
                    c-9.373-9.373-24.569-9.373-33.941 0l-16.971 16.971c-9.373 9.372-9.373 24.568 0 33.941z" />
                </svg>
            </span>
			<span class="sr-only">{!! __('Sledeći', THEME_TEXT_DOMAIN) !!}</span>
		</a>
	@endif
</div>

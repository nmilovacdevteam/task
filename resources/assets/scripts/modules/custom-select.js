/**
 * Create custom select
 */
export default class CreateCustomSelect {
	/**
	 *
	 * @param select_input
	 * @param hideInput
	 */
	constructor( select_input, hideInput = true ) {
		this.select_input = typeof ( select_input ) == 'string' ? document.querySelector( select_input ) : select_input;

		// Check if there is select input, otherwise output warning
		if ( !this.select_input ) {
			console.warn( 'Cannot find select element: ', select_input );
			return;
		}

		// Hide real select input by default
		if ( hideInput ) {
			this.select_input.style.display = 'none';
		}

		this.select_options = this.select_input.options;
		this.input_id       = this.select_input.id || Date.now();
		this.arrow          = `<span class="icon">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 280" width="15" height="10">
								<path fill="currentColor" d="M207.1,265.3l-36.5-36.5L12.7,70.9c-4-4-6.3-9-6.9-14.2C5,49.7,7.3,42.4,12.7,37l22.2-22.2c6.4-6.4,15.5-8.4,23.6-6.1
								c4,1.1,7.7,3.3,10.7,6.5l85.8,90.1l29,29l40.5,40.5l39.5-39.5v0.3l6.8-7.1l108-113.4c3.5-3.6,7.8-6,12.3-6.9
								c4.4-0.9,8.9-0.6,13.2,0.9c3.2,1.2,6.2,3,8.8,5.7L435.3,37c2.6,2.6,4.4,5.5,5.6,8.7c1.4,3.9,1.8,8.1,1.1,12.2c-0.8,4.8-3,9.3-6.7,13
								L278,228.2l-37,37C231.6,274.7,216.4,274.7,207.1,265.3z"/></svg></span>`;
		this.CreateLayout();

		document.addEventListener( 'click', e => {
			// Get openned custom select if any
			const openned_select = document.querySelector( '.custom-select.show' );

			// Change select value if some of the options from custom select are clicked and close custom select
			if ( e.target.closest( '.custom-select__options .option' ) && openned_select ) {
				openned_select.classList.remove( 'show' );
				this.ChangeValue( e.target.closest( '.custom-select__options .option' ) );
				return;
			}

			// Close custom select when clicked anywhere except custom select's showing option, when it is pressed it should open the custom select
			if ( !e.target.closest( '.option--selected' ) ) {
				openned_select && openned_select.classList.remove( 'show' );
			}

		} );

		// Change selected value in custom select when real select changes selected option
		this.select_input.addEventListener( 'change', () => {
			this.select_input.nextElementSibling.querySelector( '.option--selected' ).innerHTML = this.select_input.selectedOptions[ 0 ].textContent + this.arrow;

			// Need to use timeout to wait for select input change and then pickup it's values
			setTimeout( this.ResetSelectValues, 100, this.select_input );
		} );
	}

	ChangeValue( selected_option ) {
		// Get select input by name attribute from custom select's data-name attribute
		const select_input = document.querySelector( `[name="${ selected_option.parentElement.dataset.name }"]` ),
		      // Create custom change event, because there is no normal event when changing input value with js
		      event        = document.createEvent( 'HTMLEvents' );

		// Change selected value in real select
		select_input.querySelector( `[value="${ selected_option.dataset.value }"]` ).selected = true;

		// Emmit change event on real select input
		event.initEvent( 'change', true, false );
		select_input.dispatchEvent( event );
	}

	ResetSelectValues( input ) {
		// Get all select inputs and loop through, change old values in custom select to new ones
		const custom_select                  = input.nextElementSibling,
		      custom_select_option_container = custom_select.querySelector( '.custom-select__options' );

		custom_select_option_container.innerHTML = CreateCustomSelect.CreateOptions( input.options );
	}

	// Create custom select layout
	CreateLayout() {
		let layout = `<div class="custom-select" id="select-${ this.input_id }">
						<span class="option option--selected" data-togle="#select-${ this.input_id }">
                            ${ this.select_input.selectedOptions[ 0 ].textContent }
                            ${ this.arrow }                                    
                        </span>
						<div id="options-${ this.input_id }" class="custom-select__options" data-name="${ this.select_input.name }">`;

		layout += CreateCustomSelect.CreateOptions( this.select_input.options );
		layout += '</div></div>';

		this.select_input.insertAdjacentHTML( 'afterend', layout );
	}

	// Compose options for custom select
	static CreateOptions( options ) {
		let layout = '';

		Array.from( options ).forEach( option => {
			// Populate custom select with all option except the first which is label
			layout += `<span class="option ${ option.className || '' }" id="${ option.id || '' }" data-value="${ option.value }">${ option.textContent }</span>`;
		} );

		return layout;
	}
}

/**
 * Use only if you're not using woocommerce js for changing variation data
 * @constructor
 */
function GetVariationData( variation_output ) {

	const variation_id_input = document.querySelector( '.variation_id' ),
	      variation_select   = document.querySelectorAll( '#product-modal select' ),
	      form_el            = document.querySelector( '#product-modal form' ),
	      selected_options   = Array.from( variation_select ).map( select => select.selectedOptions[ 0 ].value );
	let variations           = JSON.parse( form_el.dataset.product_variations );

	// Clean text from previous variation
	variation_output.innerHTML = '';
	variation_id_input.value   = '';

	// Search variation only if all select inputs are selected
	if ( selected_options.includes( '' ) ) return;

	// Filter variations based on select input's values
	selected_options.forEach( option => {
		variations = variations.filter( variation => {
			let attributes = Object.values( variation.attributes );
			return attributes.includes( option );
		} );
	} );

	// If there is no combination write message for no selected variation
	if ( !variations.length ) {
		variation_output.innerHTML = `
		<p>${ variation_output.dataset.no_variation }</p>
		`;
		return;
	}

	// If there is variation, then get necessary information and print to user
	const { price_html, variation_id, sku, variation_description, image } = variations[ 0 ];

	variation_output.innerHTML = `
	<div class="woocommerce-variation-description">${ variation_description }</div>
	<div class="woocommerce-variation-price">${ price_html }</div>`;

	variation_output.classList.add( 'show' );

	// Change variation id in hidden input
	variation_id_input.value = variation_id;
}

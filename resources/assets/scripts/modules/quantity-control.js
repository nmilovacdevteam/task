import { Emmit } from './utils';

/**
 * Change product quantity with custom buttons
 * Buttons need to have classes btn--qty, btn--minus/btn--plus and
 * also data-target attribute with id selector for the input
 * @constructor
 */
export default class QuantityControl {
	constructor() {
		let interval, count;

		document.addEventListener( 'mousedown', e => {
			if ( !e.target.closest( '.btn--qty' ) ) return;
			const closest_el = e.target.closest( '.btn--qty' ),
			      qty_input  = document.querySelector( closest_el.dataset.target );
			count            = setTimeout( () => {
				interval = setInterval( this.changeQuantity, 50, qty_input, closest_el );
			}, 100 );

		} );

		document.addEventListener( 'mouseup', e => {
			if ( !e.target.closest( '.btn--qty' ) ) return;
			clearTimeout( count );
			clearInterval( interval );
		} );

		document.addEventListener( 'click', e => {
			if ( !e.target.closest( '.btn--qty' ) ) return;
			const closest_el = e.target.closest( '.btn--qty' ),
			      qty_input  = document.querySelector( closest_el.dataset.target );

			this.changeQuantity( qty_input, closest_el );

		} );
	}

	changeQuantity( qty_input, pressed_element ) {
		const min_val       = +qty_input.min || 1,
		      max_val       = +qty_input.max || Number.MAX_SAFE_INTEGER,
		      step          = +qty_input.step || 1,
		      current_value = +qty_input.value;
		let val             = 0;

		if ( pressed_element.classList.contains( 'btn--minus' ) ) {
			val = current_value - step;
			val = val <= min_val ? min_val : val;
		} else if ( pressed_element.classList.contains( 'btn--plus' ) ) {
			val = current_value + step;
			val = val >= max_val ? max_val : val;
		}

		qty_input.value = val;

		new Emmit( qty_input, { type: 'change' } );
	}
}

/**
 * Pulse animation
 *
 * @param element
 * @param options
 */
function pulse( element, { duration = 2300, iterations = Infinity, easing = "ease-in-out", delay = 0 } ) {
    element.animate( [
                         {
                             transform: "scale3d( 1, 1, 1 )"
                         },

                         {
                             transform: "scale3d( 0.9, 0.9, 0.9 )"
                         },

                         {
                             transform: "scale3d( 1, 1, 1 )"
                         },
                     ], {
                         duration  : duration,
                         iterations: iterations,
                         easing    : easing,
                         delay     : delay
                     } )
}

export {
    pulse
}

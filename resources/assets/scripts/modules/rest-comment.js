import { CreateFormData } from './utils';

window.addEventListener( 'load', () => {
	const comment_form = document.querySelector( '#comment-form' );

	comment_form && comment_form.addEventListener( 'submit', e => {
		e.preventDefault();
		const url = '/wp-json/add/comment';
		add_comment( url, comment_form );
	} );

	document.addEventListener( 'click', e => {
		if ( !e.target.closest( '.reply-btn' ) ) return;
		const closest_element = e.target.closest( '.reply-btn' ),
		      reply_form      = closest_element.nextElementSibling,
		      post_id         = reply_form.dataset.post_id,
		      comment_id      = reply_form.dataset.comment_id,
		      html            = `					
								<div class="form-group">
						            <label for="name" class="sr-only">Ime i prezime* obavezno</label>
						            <input class="form-control" id="name" name="name" type="text" required placeholder="Ime i prezime*">
						        </div>
						        <div class="form-group">
						            <label for="email" class="sr-only">Email* obavezno</label>
						            <input class="form-control" id="email" name="email" type="email" required placeholder="Email*">
						        </div>
						        <div class="form-group">
						            <label for="comment" class="sr-only">Komentar* obavezno</label>
						            <textarea class="form-control" id="comment" name="comment" required placeholder="Komentar*"></textarea>
						        </div>
						        <div class="form-group">
						            <button type="submit" class="btn btn--blue">Odgovori</button>
						            <input type="hidden" name="post_id" id="postId" value="${ post_id }" />
						            <input type="hidden" name="comment_id" id="comment_id" value="${ comment_id }" />
						        </div>
						        <div class="form-notice"></div>`;
		reply_form.insertAdjacentHTML( 'beforeend', html );
		reply_form.classList.toggle( 'active' );

		reply_form.addEventListener( 'submit', e => {
			e.preventDefault();
			let url = '/wp-json/add/reply';
			add_comment( url, reply_form );
		} );
	} );
} );

function add_comment( url, form ) {
	const data          = CreateFormData( form.elements ),
	      notice_output = form.querySelector( '.form-notice' ),
	      submit_btn    = form.querySelector( '[type="submit"]' );

	// Start animating submit btn
	submit_btn && submit_btn.classList.add( 'loading' );

	fetch( url, {
		method: 'post',
		body:   data,
	} )
		.then( response => response.json() )
		.then( json => {
			notice_output.innerHTML = `<p>${ json }</p>`;

			// Stop animating submit btn
			submit_btn && submit_btn.classList.remove( 'loading' );

			// Empty values after submitting comment
			Array.from( form.elements ).forEach( el => el.value = '' );
		} )
		.catch( error => {
			submit_btn && submit_btn.classList.remove( 'loading' );
			console.error( 'REST API error:', error );
		} );
}
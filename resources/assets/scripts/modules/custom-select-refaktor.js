/**
 * Create custom select
 */
class CreateCustomSelect {
	/**
	 *
	 * @param select_input
	 * @param hideInput
	 */
	constructor( select_input, hideInput = true ) {
		this.select_inputs = document.querySelectorAll( select_input );
		this.selects       = {};
		this.opennedSelect = undefined;

		// Check if there is select input, otherwise output warning
		if ( !this.select_inputs.length ) {
			console.warn( 'Cannot find select elements: ', select_input );
			return;
		}

		this.select_inputs.forEach( input => {
			const curr_input = {
				[ input.name ]: {
					select: {
						el:       input,
						options:  input.options,
						selected: input.selectedOptions[ 0 ],
						input_id: input.id || ( `${ input.name }-${ Date.now() }` )
					}
				}
			};

			this.selects = { ...this.selects, ...curr_input };

			// Hide real select input by default
			if ( hideInput ) {
				input.style.display = 'none';
			}

			this.CreateLayout( this.selects[ input.name ].select );
		} );


		// Attach click event listener to custom select
		this.HandleClick();


		// TODO Srediti promeni vrednosti na klik
		// document.addEventListener( 'click', e => {
		// 	// Get openned custom select if any
		// 	const openned_select = document.querySelector( '.custom-select.show' );
		//
		// 	// Change select value if some of the options from custom select are clicked and close custom select
		// 	if ( e.target.closest( '.custom-select__options .option' ) && openned_select ) {
		// 		openned_select.classList.remove( 'show' );
		// 		this.ChangeValue( e.target.closest( '.custom-select__options .option' ) );
		// 		return;
		// 	}
		//
		// 	// Close custom select when clicked anywhere except custom select's showing option, when it is pressed it should open the custom select
		// 	if ( !e.target.closest( '.option--selected' ) ) {
		// 		openned_select && openned_select.classList.remove( 'show' );
		// 	}
		//
		// } );

		// Change selected value in custom select when real select changes selected option.
		// This is crucial for woocommerce variations, because after selecting one attribute,
		// woocommerce changes select inputs with only available options
		document.addEventListener( 'change', e => {
			if ( !this.select_inputs[ e.target.name ] ) return;

			this.select_inputs[ e.target.name ].customSelect.selected.textContent = this.select_inputs[ e.target.name ].select.selected.textContent;

			// Need to use timeout to wait for select input change and then pickup it's values
			setTimeout( this.ResetSelectValues, 100, this.select_inputs[ e.target.name ] );
		} );
	}

	ChangeValue( selected_option ) {
		// Get select input by name attribute from custom select's data-name attribute
		const select_input = document.querySelector( `[name="${ selected_option.parentElement.dataset.name }"]` ),
		      // Create custom change event, because there is no normal event when changing input value with js
		      event        = document.createEvent( 'HTMLEvents' );

		// Change selected value in real select
		select_input.querySelector( `[value="${ selected_option.dataset.value }"]` ).selected = true;

		// Emmit change event on real select input
		event.initEvent( 'change', true, false );
		select_input.dispatchEvent( event );
	}

// Promeni stari select i custom select sa novom verzijom i ažurirajm html za custom select
	ResetSelectValues( inputName ) {
		const select_obj                                          = this.select_inputs[ inputName ].select;
		this.select_inputs[ inputName ].customSelect.el.innerHTML = this.CreateOptions( select_obj.el );
	}

	CreateLayout( input ) {
		const custom_select = document.createElement( 'div' );
		custom_select.classList.add( 'custom-select' );
		custom_select.id           = `select-${ input.input_id }`;
		custom_select.dataset.name = input.el.name;

		let layout = `<span class="option option--selected" data-custom_select="#select-${ input.input_id }">${ input.el.selectedOptions[ 0 ].textContent }</span>
						<div id="options-${ input.input_id }" class="custom-select__options">`;

		// Compose options
		layout += this.CreateOptions( input );

		layout += '</div>';

		custom_select.innerHTML = layout;
		input.el.parentElement.insertBefore( custom_select, input.nextElementSibling );

		const custom_select_el        = {
			customSelect: {
				el:       custom_select,
				options:  custom_select.querySelectorAll( '.custom-select__options .option' ),
				selected: custom_select.querySelector( '.option--selected' ),
				input_id: custom_select.id,
				openned:  false
			}
		};
		this.selects[ input.el.name ] = { ...this.selects[ input.el.name ], ...custom_select_el };
	}

	CreateOptions( input ) {
		let layout = '';
		Array.from( input.options ).forEach( option => {
			// Populate custom select with all option except the first which is label
			layout += `<span class="option ${ option.className || '' }" id="${ option.id || '' }" data-value="${ option.value || '' }">${ option.textContent || '' }</span>`;
		} );

		return layout;
	}

	HandleClick() {
		document.addEventListener( 'click', e => {
			// if ( !e.target.closest( '.custom-select' ) ) return;

			const clickedEl = e.target.closest( '.custom-select' );

			// If user clicked outside of the custom select, close the previous opened select if any
			if ( !clickedEl ) {
				if ( this.opennedSelect ) {
					this.opennedSelect.el.classList.remove( 'show' );
					this.opennedSelect.openned = false;
					this.opennedSelect         = undefined;
				}

				return;
			}

			const selects = this.selects[ clickedEl.dataset.name ];

			// If there is openned select, close it
			if ( this.opennedSelect && this.opennedSelect.el !== selects.customSelect.el ) {
				this.opennedSelect.el.classList.remove( 'show' );
				this.opennedSelect.openned = false;
				this.opennedSelect         = undefined;
			}

			// Check if clicked select exists in this instance
			if ( !selects ) return;

			if ( e.target.closest( '.custom-select__options .option' ) ) {
				selects.customSelect.selected.textContent = e.target.closest( '.custom-select__options .option' ).textContent;
				selects.customSelect.el.classList.remove( 'show' );
				selects.customSelect.openned = false;
				this.opennedSelect           = undefined;
			}

			// If user clicks on selected option open select if closed, otherwise close it
			if ( e.target.closest( '.option--selected' ) ) {
				// Show select that was clicked
				selects.customSelect.el.classList.toggle( 'show' );
				selects.customSelect.openned = !selects.customSelect.openned;
				this.opennedSelect           = selects.customSelect.openned ? selects.customSelect : undefined;
			}
		} );
	}
}

/**
 * Use only if you're not using woocommerce js for changing variation data
 * @constructor
 */
function GetVariationData( variation_output ) {

	const variation_id_input = document.querySelector( '.variation_id' ),
	      variation_select   = document.querySelectorAll( '#product-modal select' ),
	      form_el            = document.querySelector( '#product-modal form' ),
	      selected_options   = Array.from( variation_select ).map( select => select.selectedOptions[ 0 ].value );
	let variations           = JSON.parse( form_el.dataset.product_variations );

	// Clean text from previous variation
	variation_output.innerHTML = '';
	variation_id_input.value   = '';

	// Search variation only if all select inputs are selected
	if ( selected_options.includes( '' ) ) return;

	// Filter variations based on select input's values
	selected_options.forEach( option => {
		variations = variations.filter( variation => {
			let attributes = Object.values( variation.attributes );
			return attributes.includes( option );
		} );
	} );

	// If there is no combination write message for no selected variation
	if ( !variations.length ) {
		variation_output.innerHTML = `
		<p>${ variation_output.dataset.no_variation }</p>
		`;
		return;
	}

	// If there is variation, then get necessary information and print to user
	const { price_html, variation_id, sku, variation_description, image } = variations[ 0 ];

	variation_output.innerHTML = `
	<div class="woocommerce-variation-description">${ variation_description }</div>
	<div class="woocommerce-variation-price">${ price_html }</div>`;

	variation_output.classList.add( 'show' );

	// Change variation id in hidden input
	variation_id_input.value = variation_id;
}

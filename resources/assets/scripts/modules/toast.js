/**
 * Toast class for notifications
 *
 * Provide selector or HTML NodeList with toasts
 * Auto show and auto hide for notices can be controlled
 * with data attributes on toasts or set globally with initialisation or if not present than defaults are used
 */
export default class Toast {
	/**
	 *
	 * @param toast {NodeList | string}
	 * @param show_time {number}
	 * @param hide_time {number}
	 */
	constructor( toast, show_time = 350, hide_time = 5000 ) {
		this.toasts    = toast instanceof NodeList ? toast : document.querySelectorAll( toast );
		this.show_time = show_time;
		this.hide_time = hide_time;

		if ( !this.toasts.length ) {
			console.warn( 'Cannot find toasts: ', toast );
			return;
		}

		this.init();
	}

	/**
	 * Function for controlling notices
	 * @param notice {Element}
	 */
	showNotice( notice ) {
		if ( !notice ) return;

		notice.classList.add( 'show' );
	}

	/**
	 * Function for controlling notices
	 * @param notice {Element}
	 */
	hideNotice( notice ) {
		if ( !notice ) return;

		notice.classList.remove( 'show' );
	}

	init() {
		this.toasts.forEach( toast => {
			this.show_time = +toast.dataset.show_time || this.show_time;
			this.hide_time = ( +toast.dataset.hide_time || this.hide_time ) + this.show_time;
			this.autohide  = toast.dataset.autohide === 'true';

			setTimeout( this.showNotice, this.show_time, toast );

			if ( this.autohide ) {
				setTimeout( this.hideNotice, this.hide_time, toast );
			}
		} );
	}
}
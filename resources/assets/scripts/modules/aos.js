export class AOSLoad {
	constructor( elements = document.querySelectorAll( '[data-aos]' ), options = { root: null, rootMargin: '0px', treshold: this.buildThresholdList() } ) {
		this.elements = elements;
		this.options  = options;

		if ( !this.elements.length ) {
			console.error( 'There are no elements to load: ', this.elements );
			return;
		}

		this.init();
	}

	buildThresholdList( numSteps = 10 ) {
		let thresholds = [];

		for ( let i = 1.0; i <= numSteps; i++ ) {
			let ratio = i / numSteps;
			thresholds.push( ratio );
		}

		thresholds.push( 0 );
		return thresholds;
	}

	composeObserver() {
		this.observer = new IntersectionObserver( entries => {
			entries.forEach( entry => {
				if ( entry.intersectionRatio > 0.7 && window.matchMedia( '(min-width: 768px)' ).matches ) {
					entry.target.classList.add( 'loaded' );
				} else if ( entry.isIntersecting ) {
					entry.target.classList.add( 'loaded' );
				}
				if ( entry.intersectionRatio === 1 ) {
					this.observer.unobserve( entry.target );
				}
			} );
		}, this.options );
	}

	init() {
		this.composeObserver();

		if ( 'IntersectionObserver' in window ) {
			this.elements.forEach( element => {
				this.observer.observe( element );
			} );
		}
	}
}
/* eslint-disable */

const cssnanoConfig = {
	preset: [ 'default', { discardComments: { removeAll: true } } ]
};
// const autoprefixer  = require( 'autoprefixer' );
// const cssMquery     = require( 'css-mquery-packer' );
// const cssNano       = require( 'cssnano' );
// const { argv }      = require( 'yargs' );
// const isProduction  = !!( ( argv.env && argv.env.production ) || argv.p );

module.exports = ( { file, options } ) => {
	// return {
	// 	parser:  options.enabled.optimize ? 'postcss-safe-parser' : undefined,
	// 	plugins: (isProduction ? [
	// 		autoprefixer,
	// 		cssNano,
	// 		cssMquery
	// 	] : {
	// 		autoprefixer: true,
	// 		cssnano:      options.enabled.optimize ? cssnanoConfig : false,
	// 	})
	// };
	return {
		parser:  options.enabled.optimize ? 'postcss-safe-parser' : undefined,
		plugins: {
			autoprefixer: true,
			cssnano:      options.enabled.optimize ? cssnanoConfig : false,
		}
	};
};

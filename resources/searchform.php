<form action="/" id="search-form" autocomplete="off">
	<div class="form-group form-group--append">
		<label for="search" class="sr-only"><?php _e( 'Unesite traženi pojam', THEME_TEXT_DOMAIN ) ?></label>
		<input type="text" name="s" id="search" class="form-control search-input" value="" placeholder="<?php _e( 'Unesite traženi pojam', THEME_TEXT_DOMAIN ) ?>">
		<button type="button" class="btn btn-search" aria-label="<?php _e( 'Pretraži pojam', THEME_TEXT_DOMAIN ) ?>">
			<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
				<path fill="#231F20"
				      d="M12.75 7.126c.001-3.098-2.525-5.624-5.624-5.624-3.098 0-5.624 2.525-5.625 5.623 0 3.098 2.526 5.624 5.625 5.625 3.098 0 5.624-2.525 5.625-5.624
				      m-1.137 5.534c-1.761 1.363-3.726 1.871-5.902 1.443-1.77-.35-3.202-1.276-4.287-2.714-2.105-2.792-1.874-6.757.667-9.297 2.55-2.548 6.52-2.768 9.303
				      -.66 1.597 1.21 2.547 2.821 2.795 4.815.249 1.992-.288 3.776-1.534 5.368.022.016.048.031.069.051l5.125 5.12c.116.114.183.243.133.407-.016.056
				      -.047.113-.087.155-.175.182-.351.362-.534.535-.167.16-.372.154-.544-.005l-.048-.047-5.102-5.104c-.02-.02-.037-.045-.054-.067" />
			</svg>
		</button>
	</div>
</form>

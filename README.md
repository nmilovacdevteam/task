# Starter tema za WordPress

## !!! Važno !!! Tema nakon prve aktivacije pravi default stranice, briše Hello World i Sample Page, menja format datuma i vremena, postavlja statičke strane za  prikaz početne strane i bloga. !!! Važno !!!

## Tehnologije

------

*   HTML5
*   CSS3
*   Bootstrap 4
*   SCSS
*   JS ES6+
*   JQuery
*   Blade
*   ACF Builder
*   Sage tema
*   Soil plugin



## Zahtevi

------

Obavezno mora sve biti instalirano.

*   WordPress >= 5.3.0
*   PHP >= 7.1
*   Composer
*   Node >=12.0.0
*   Yarn



## Dokumentacije
------

Sage oficijalna dokumentacija na [github-u](https://github.com/roots/sage) ili njihovom [sajtu ](https://roots.io/sage/docs/).

[ACF Builder](https://github.com/Log1x/acf-builder-cheatsheet) oficijalna dokumentacija na github-u.


## Dodaci teme
1. AOS - Animacije na skroll
2. Cookie - postavljanje i provera kolačića
3. Custom select - pravljenje custom selekta
4. Leader line - dodatak za spajanje html elemenata. [Dokumentacija](https://anseki.github.io/leader-line/)
5. Quantity control - custom input za količinu 


## Postavljanje teme

Pre preuzimanja sa bitbucket-a, potrebno je imati instaliran WordPress. U folderu `ime_sajta/wp-content/themes`, potrebno je otvoriti **terminal** ili **komandnu liniju** i skinuti temu sa git-a.

Nakon povlačenja sa git-a potrebno je uraditi sledeće:
### Nakon toga potrebno je pokrenuti sledeće komande:

```bash
composer install
yarn
yarn build
```

**Ako se pojavi greška tokom pokretanja komande `yarn build`, najčešće bude problem sa node-sass, potrebno ga je samo ponovo instalirati, to radimo sledećom komandom:**

```bash
yarn add node-sass -D
```

**Nakon toga ponovo uradimo bildovanje fajlova**

```bash
yarn build
```
### Build commands

* `yarn start` — Compile assets when file changes are made, start Browsersync session
* `yarn build` — Compile and optimize the files in your assets directory
* `yarn build:production` — Compile assets for production

## Documentation

* [Sage documentation](https://roots.io/sage/docs/)
* [Controller documentation](https://github.com/soberwp/controller#usage)
